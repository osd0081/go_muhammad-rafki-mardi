package main

import (
	"fmt"
)

func fibo(n int) int {
	if n < 0{
		return 0
	}
	if n <= 1 {
		return n
	}
	nMinSatu , nMinDua := 1,0
	for i := 2; i <= n; i++ {
		nMinSatu , nMinDua = (nMinSatu + nMinDua), nMinSatu
	}
	return nMinSatu
}

func main()  {
	fmt.Println(fibo(1))	
	fmt.Println(fibo(2))	
	fmt.Println(fibo(3))	
	fmt.Println(fibo(4))	
	fmt.Println(fibo(5))	
	fmt.Println(fibo(6))	
	fmt.Println(fibo(7))	
	fmt.Println(fibo(8))	
	fmt.Println(fibo(9))	
	fmt.Println(fibo(10))	
}
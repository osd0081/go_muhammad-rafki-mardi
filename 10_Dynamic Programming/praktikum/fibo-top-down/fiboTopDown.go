package main

import (
	"fmt"
)

var memoFibTopDown = make(map[int]int)

func fibo(n int) int {

	if _,exist := memoFibTopDown[n]; exist{
		return memoFibTopDown[n]
	}

	if n <=1 {
		memoFibTopDown[n] = n
		return memoFibTopDown[n]
	}
	memoFibTopDown[n] = fibo(n-1)+fibo(n-2)
	return memoFibTopDown[n]
}

func main()  {
	fmt.Println(fibo(1))
	fmt.Println(fibo(2))
	fmt.Println(fibo(3))
	fmt.Println(fibo(4))
	fmt.Println(fibo(5))
	fmt.Println(fibo(6))
	fmt.Println(fibo(7))
	fmt.Println(fibo(8))
	fmt.Println(fibo(9))
	fmt.Println(fibo(10))
}
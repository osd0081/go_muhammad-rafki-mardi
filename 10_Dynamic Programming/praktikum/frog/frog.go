package main

/* credit https://www.youtube.com/watch?v=FyOglVmGiRc */

import (
	"fmt"
)

func Frog(jumps []int) int {

	if len(jumps) == 2 {
		res := jumps[0] - jumps[1]
		if res < 0 {return res*-1}
		return res
	}

	var min int
	var dp = make([]int,len(jumps))
	
	min = int(^uint(0)>>1)

	for in,_ := range jumps {
		dp[in] = min
	}

	for i:=0 ; i < len(jumps)-1 ;i++{
		stepOne := jumps[i] - jumps[i+1]
		if stepOne < 0 {stepOne*=-1}
		if dp[i] != min {stepOne += dp[i]}
		if stepOne < dp[i+1] {dp[i+1] = stepOne}

		if i != len(jumps) - 2{
			stepTwo := jumps[i] - jumps[i+2]
			if stepTwo < 0 {stepTwo*=-1}
			if dp[i] != min {stepTwo += dp[i]}
			if stepTwo < dp[i+2] {dp[i+2] = stepTwo}
		}
	}
	return dp[len(dp)-1]
}

func main()  {
	fmt.Println(Frog([]int{10,30,40,20}))
	fmt.Println(Frog([]int{30,10,60,10,60,50}))
}
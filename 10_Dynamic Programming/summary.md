# (10) Dynamic Programming

- [Summary](#summary)
- [Praktikum](#praktikum)


# Summary

A. Dynamic Programming

- Pendekatan pemecahan permasalahan yang memecah permasalahan tersebut menjadi submasalah dan menggunakan fakta dari masalah yang memiliki solusi optimal dari keseluruhan masalah.

|Karakteristik DP|Deskripsi|
|---|---|
|*Overlapping subproblem*|Masalah original tersusun atas submasalah yang lebih kecil|
|*Optimal Substructure Property*|Pada DP, suatu masalah haruslah memiliki struktur masalah & sub-masalah yang konsisten & optimal|

- Pendekatan DP

|Pendekatan|Deskripsi|
|---|---|
|*Top-down memoization*|ketika sudah menyelesaikan submasalah, kita tidak perlu menghitung ulang, cukup menyimpan nilai tersebut kedalam sebuah memory, misal rekursif, high memory cycle, low cpu cycle|
|*Bottom-up Tabulation*|Menghindari rekursif, pemecahan masalah dimulai dari base case yang mana nilai tersebut sudah langsung merupakan optimal sub masalah, low memory cyccle, high cpu|

# Praktikum

[Hasil Submisi](https://www.interviewzen.com/interview/b99b8666-2f07-4b8a-8667-de0718bddce5)

## 1. Fibo Top-Down

[Source code](./praktikum/fibo-top-down/fiboTopDown.go)

- Menggunakan rekursif untuk menghitung nilai fibonacci ke `N`
- Menggunakan variabel global map untuk melakuakan `pre compute` jika dimasukan input selanjutnya.
- Special case ketika nilai fibo ke `N` ada pada map, maka kembalikan nilai map teresbut
- Membuat special case, ketika `N<=1`, masukan nilia tersebut kedalam map dan kembalikan nilai map tersebut
- Jika tidak, dilakukan penambahan elemen pada map dengan memanggil fungsi `fib(N-1)+fib(N-2)`

![hasil](./screenshots/fibTD.png)

## 2. Fibo Bottom-Up

[Source code](./praktikum/fibo-bottom-up/fiboBottomUp.go)

- Mengeluarkan bilangan fibonacci ke `N` dengan metode *
- Membuat special case, ketika `N<=1` kembalikan nilai `N`
- Dibuat dua buah variable yang memegang nilai `fib(N-1)` dan `fib(N-2)`
- Menggunakan iterasi mulai dari `2` hingga `N`, nilai `fib(N-1) = fib(N-1) + fib(N-2)` dan nilai `fib(N-2)=fib(N-1)`
- Kembalikan nilai `fib(N-1)`

![hasil](./screenshots/fibBU.png)

## 3. Frog

[Source code](./praktikum/frog/frog.go)

- [Credit] https://www.youtube.com/watch?v=FyOglVmGiRc
- Menggunakan metode `bottom-top`.
- Disiapkan sebuah array `DP` yang diisi dengan nilai `MAX_INT` untuk menyimpan minimal keluaran untuk mencapai batu tersebut.
- Dicek setiap batu dan dikalkulasikan keluaran yang didapat untuk lompat ke batu `N+1` dan `N+2` (special case ketika terdapat batu `N+2`) dan disimpa ke `stepOne` dan `stepTwo` berturut-turut.
- Sebelum dicek dengan `DP[i+1]` dan `DP[i+2]` dilalui tahap berikut:
    - jika hasil keluaran negatif, jadikan positif
    - jika `DP[i]` non `INT_MAX`, tambahkan nilai tersebut ke `stepOne` dan `stepTwo`
- jika nilai `stepOne` dan `stepTwo` kecil dari `DP` yang berkorespondensi tukar nilainya, jika tidak lanjut iterasi berikutnya.

![hasil](./screenshots/frog.png)

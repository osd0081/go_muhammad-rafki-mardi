package main

import (
	"fmt"
	"sync"
)

var theChar = make(map[rune]int)

func main()  {
	theWord := "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"

	var wg sync.WaitGroup
	
	var countChar = func (char rune,words string) {

		var ammount int
		for _,el := range words{
			if char == el {
				ammount++
			}
		}

		if  (65 <= char && char <=  90) || (97 <= char && char <=  122){
			theChar[char] = ammount
			wg.Done()
			fmt.Println(string(char),":",ammount)
		}else {
			wg.Done()
		}
	}

	for _,el := range theWord{
		if _,exist := theChar[el];!exist{
			wg.Add(1)
			go countChar(rune(el),theWord)
		}
		wg.Wait()
	}

}
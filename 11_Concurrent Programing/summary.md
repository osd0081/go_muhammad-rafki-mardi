# (11) Concurrent Programing

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

|Sequential|Parallel(Simultan)|Concurrent(Independen)|
|---|---|---|
|Untuk menjalankan tugas selanjutnya, tugas sebelumnya harus selesai|Tugas tugas dapat berjalan secara simultan (membutuhkan mesin mulit core)|Banyak tugas seolah olah dilaksanakan secra bersamaan dan bisa saja muncul bersamaam|

- Eksekusi secara konkuren dengan `Goroutine`
- Main merupakan konkuren utama, sedangkan sukonkuren task berjalan pada `Channels`
- Pengaturan task pada `Channel` tersebut diatur dengan `Select`

## Goroutines

- Sintaks `go`
- Merupakan fungsi atau method yang berjalan secara konkuren (independen) dengan fungsi dan method lainnya
- Sehingga dalam keluran sumber daya `goroutine < thread`
- Pengaturan jumlah thread untuk menjalankan `goroutine` dapat diatur dengan argumen `time GOMAXPROCX=N` dengan `N` adalah jumlah thread.

## Channels

- Sintaks `chan` dengan `make()`
- Mewujudkan komunikasi antara `Goroutine`
- `chan` hanya dapat mengembalikan satu tipe data
- Channel dapat berupa `Buffered` dan `Unbuffered`, bedanya `Unbuffered` perlu `goroutine` lain jika `buffered`.

## Select

- Bagaimana komunikasi antar `Channels`
- `Select` memiliki sintaks yang mirip dengan `swtict`

## Race Condition

- Salah satu kelemaha konkurensi
- Terjadi ketika dua atau lebih `Goroutine` memakai satu sumber daya secara bersamaan.
- Cara mengatasi 

|Metode|Deskripsi|
|---|---|
|Waitgroups (sync.Waitgroups)|Mengunci akses perubahan terhadap data, hal ini terjadi secara sinkronus|
|Blocking ()|Untuk sinkronisasi antar channel|
|Mutex(Mutual Exclusion) (sync.Mutex) ()|Write tidak terjadi secara bersamaan|

# Praktikum

## Letter Frequency

[Source code](./praktikum/letter-frequency/letterFrequency.go)

- Dibuat inner function untuk menghitung jumlah karakter dalam sebuah kata
- Dibuat map untuk menghindari komputasi yang tidak diperlukan
- Digunakan `Goroutine` dan `WaitGroup` guna menghindari `racing` dalam `read` atau `write` map
- `Goroutine` dipanggil sebanyak panjang kalimat, dan dipanggil `WaitGroup` ketika karakter yang akan dicek tidak terdapat pada map
- Saat dicek, dilakukan cetak untuk karakter alfabet selain (alfabet) langsung `Done`.

![Hasil](./screenshots/concurrent.png)
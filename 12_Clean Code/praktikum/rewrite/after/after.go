package main

type kendaraan struct {
	totalRoda int
	kecepatan int
}

type mobil struct {
	kendaraan
}

/* 
* Method Public (m *mobil) Berjalan().
* 
* Menambah kecepatan mobil
*/

func (m *mobil) Berjalan() {
	const PERCEPATAN = 10
	m.akselerasi(PERCEPATAN)
}

/* 
* Method Private (m *mobil) akselerasi().
* 
* menambah kecepatan kendaraan
* @param int kecepatanTambah - Properti PERCEPATAN dari Method Berjalan
*/

func (m *mobil) akselerasi(kecepatanTambah int) {
	m.kecepatan = m.kecepatan + kecepatanTambah
}

func main() {
	mobilCepat := mobil{}
	mobilCepat.Berjalan()
	mobilCepat.Berjalan()
	mobilCepat.Berjalan()

	mobilLamban := mobil{}
	mobilLamban.Berjalan()
}

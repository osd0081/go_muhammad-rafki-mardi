# (12) Clean Code

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. Pengertian

- Merupakan istilah yang digunakan untuk kode yang mudah dibaca, difahami dan diubah oleh programmer.
- Kode yang berkerja terkadang belum termaksud _clean code_
- Baiknya seorang programmer menulis kode yang dapat dipahami oleh semua orang

## B. Tujuan

- Kemudahan ketika berkolaborasi
- Kejelasan logika fitur
- Pengembangan fitur akan lebih cepat

## C. Karakteristik

### Penamaan Mudah dipahami
- Ketika penamaan variabel/fungsi haruslah jelas.
### Mudah dieja dan dicari
- Nama fungsi/variabel baiknya mudah diingat guna ketika _revisit_
### Singkat & concise
- Nama fungsi/variabel baiknya mengandung tujuan dari variabel/fungsi
### Konsisten
- Misal, dengan prinsip penamaan CamelCase/snake_case/dst untuk variabel dls.
### Hindari penamaan konteks yang tidak perlu 
- Misal terdapat suatu `struct` atau `class` tertentu, kita harus menamakan method/atribut tanpa menambahkan nama `struct` atau `class`
### Komentar
- Gunakan komentar hanya ketika mendeskripsikan logika suatu *_block of code_*
- Komentar baiknya selalu diawal
### Function
- Dari pada memasangkan banyak parameter terhadap fungsi, baiknya gunakan `object` untuk membawa parameter
- Fungsi baiknya tidak merubah variabel global namun di simpan ke variabel lain.
- Suatu fungsi dapat memiliki sebuah `comment block` yang mendeskripsika tujuan, parameter, serta return dari fungsi tersebut
### Gunakan Konvensi
- Mengikuti style guide yang dipakai oleh perusahaan besar

### Formating

|Hal|Saran|
|---|---|
|Lebar Kode|80-120 karakter|
|Panjang satu kelas|300-500 LOC|
|Relasi|Baris kode yang berhubungan saling berdekatan|
|Deklarasi variabel|dekat denga penggunanya|
|Indentasi|konsisten|
|Tools|`prettier` atau `formatter`|

## D. Prinsip

### KISS

- Memiliki kepanjangan `keep in simple stupid`
- Prinsip `single responsibility principle`
- Fungsi atau method class baiknya kecil
- Suatu method/fungsi baiknya memiliki parameter sedikit
- Selain kecil, fungsi baiknya di jaga jumlahnya agar tidak kebanyakan

### DRY

- Memiliki kepanjangan `Don't repeat yourself`
- Jika terdapat `block of code` yang terus berulang, baiknya membuat package fungsi yang bisa digunakan lagi kedepannya

### Refactoring

- Merupakan kegatan restrukturasi kode yang dibuat denga prinsip prinsip yang sudah dijelaskan di atas.
- Mengubah struktur internal tanpa mengubah perilaku (`return`, dls)
- Membuat abstraksi
- Memecah kelas/fungsi
- Perbaikan penamaan dan lokasi kode
- Deteksi kode duplikat

# Praktikum

## 1. Analisis

```go
package before

type user struct {
	id 		 int
	username int
	password int
}

type userservice struct {
	t []user
}

func (u userservice) getallusers() []user{
	return u.t
}

func (u userservice) getuserbyid(id int) user{
	for _, r := range u.t {
		if id == r.id {
			return r
		}
	}
	return user{}
}
```

### A. Jumlah Kekurangan Dalam Program Diatas 

Secara jumlah terdapat 7 sintaks yang belum memenuhi prinsip _clean code_

|Karakterisitik|Status|
|---|---|
|Penamaan Mudah dipahami|❌|
|Konsisten|❌|

### B. Bagian yang kurang tepat

![hasil](./screenshots/analisis.PNG)

### C. Alasan tiap kekurangan

|Karakterisitik|Deskprisi|
|---|---|
|Penamaan Mudah dipahami|Penamaan variabel, baik pada parameter fungsi maupun variabel pada iterasi layaknya deberikan nama yang jelas agar mudah mengetajui tujuan dari variable tersebut.|
|Konsisten|Seperti diketahui golang berjenis `MixedCaps`, dan masing masing diantaranya memiliki tujuan tertentu. Dari method yang diberikan sebelumnya semuanya tidak dapat diakses diluar package. sehingga harus diperhatikan penamaan serta scope akses dari method yang kita buat. Hal ini juga sepurtar pemilihan `camelCase` dan `case` lainnya sepagai acuan penulisan kode|

## 2. Rewrite

### Sebelum

```go
package main

type kendaraan struct {
	totalroda		int
	kecepatanperjam	int
}

type mobil struct {
	kendaraan
}

func (m *mobil) berjalan() {
	m.tambahkecepatan(10)
}

func (m *mobil) tambahkecepatan(kecepatanbaru int){
	m.kecepatanperjam = m.kecepatanperjam + kecepatanbaru
}

func main() {
	mobilcepat := mobil{}
	mobilcepat.berjalan()
	mobilcepat.berjalan()
	mobilcepat.berjalan()

	mobillamban := mobil{}
	mobillamban.berjalan()
}
```

### Sesudah

```go
package main

type kendaraan struct {
	totalRoda int
	kecepatan int
}

type mobil struct {
	kendaraan
}

/* 
* Method Public (m *mobil) Berjalan().
* 
* Menambah kecepatan mobil
*/

func (m *mobil) Berjalan() {
	const PERCEPATAN = 10
	m.akselerasi(PERCEPATAN)
}

/* 
* Method Private (m *mobil) akselerasi().
* 
* menambah kecepatan kendaraan
* @param int kecepatanTambah - Properti PERCEPATAN dari Method Berjalan
*/

func (m *mobil) akselerasi(kecepatanTambah int) {
	m.kecepatan = m.kecepatan + kecepatanTambah
}

func main() {
	mobilCepat := mobil{}
	mobilCepat.Berjalan()
	mobilCepat.Berjalan()
	mobilCepat.Berjalan()

	mobilLamban := mobil{}
	mobilLamban.Berjalan()
}
```

Tambahan:
- `Method` _abstraction_
- Menambah konstanta `PERCEPATAN` pada method `Berjalan()`
- Menjadikan penamaan variabel dan fungsi private dengan `camelCase`
- Perbaikan nama variabel
	- kecepatanperjam ➡️ kecepatan
	- tambahkecepatan ➡️ akselerasi
	- kecepatanbaru ➡️ kecepatanTambah

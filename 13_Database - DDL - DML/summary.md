# (13) Database - DDL - DML

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. Database

- Jenis:
  - Relasional
  - Non-relasional
- Database sebagai sekumpulan data yang terorganisir
- Entitas direpresentasikan sebagai `table`
- Karakterisitik dari entitas direpresentasikan dengan `atribute`

## B. Hubungan

- One to One (ex: kamu mempunyai istri 1)
- One to Many (ex: pacarmu punya mantan banyak)
- Many to Many, perlu diketahui many to many mengakibatkan terciptanya pivot table yang berfungsi untuk menampung atribut dari data yang berulang diantara entitas. (ex: banyak customer melakukan banyak transaksi)

## C. Perintah SQL

- DDL (data definition language)
  - create ...
  - use ...
  - drop ...
  - rename ...
  - alter ...
  - add ...
- DML (data manipulation language), perintah *untuk memanipulasi data dalam tabel*
  - insert ...
  - select ...
  - update ...
  - delete ...

## D. Join

### Inner

```From tabel_kiri inner join tabel_kanan on syarat```

- Irisan dari data dua table

### Left

```From tabel_kanan right join tabel_kiri on syarat```

- Irisan dari data sintaks kanan ke kiri (semua)

### Right

```From tabel_kiri right join tabel_kanan on syarat```

- Irisan dari data sintaks kiri ke kanan (semua)

## E. Unior

- Seperti namanya menggabungkan dua buah data berdasarkan *tabel yang sama*

## F. Agregate

- Fungsi yang melakukan perhitungan terhadap seluruh kolom pada suatu atribut
  - MIN (mencari nilai minimum)
  - MAX (mencari nilai max)
  - SUM (jumlah)
  - AVG (rata rata)
  - COUNT (hitung untuk syarat tertentu atau atribut tertentu)
  - HAVING (where versi agregasi)

## G. Fucntion

- SQL terkadang juga menyediakan fungsi untuk mengolah data ketika akan dimasukan kedalam tabel, fungsi ini dibuat dengan deklarasi dan dipangil dengan trigger.

## H. Tambahan

- Untuk menyimpan file, database hanya menyimpan nilai path terhadap server

# Praktikum

## 1. Database Schema

- Penambahan entitas `operators`, `categories` dan `payments` guna memberikan `enum` pada entitas yang berelasi
- Hasil:

![hasil](./screenshots/schemaDb.png)

## 2. Data Definition Language

- Membuat table dengan query `CREATE TABLE`.
- Foreign key dicapai dengan membangun relasi dengan `ALTER TABLE` dan `ADD CONSTRAINT` dengan memperhatikan tipe data dan nama constrain.

[SCRIPT](./praktikum/alta_online_shop.sql)

- Membuat tabel `Kurir` 

```
CREATE TABLE `kurir` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
)
```

![hasil](./screenshots/tabKurir.png)

- Tambah `ongkos_dasar` di kolom tabel kurir

```
ALTER TABLE `kurir`
  ADD `ongkos_dasar` int(11)
    AFTER `name`;
```

![hasil](./screenshots/addCol.png)

- Rename `kurir` ke `shipping`

```
ALTER TABLE `kurir`
  RENAME TO `shipping`;
```

![hasil](./screenshots/renShip.png)

- Drop `shipping`

```
DROP TABLE `shipping`; 
```

![hasil](./screenshots/dropShip.png)
# (14) Join - Union - Agregasi - Subquery - Function (DBMS)

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. Join

### Inner

```From tabel_kiri inner join tabel_kanan on syarat```

- Irisan dari data dua table

### Left

```From tabel_kanan right join tabel_kiri on syarat```

- Irisan dari data sintaks kanan ke kiri (semua)

### Right

```From tabel_kiri right join tabel_kanan on syarat```

- Irisan dari data sintaks kiri ke kanan (semua)

## B. Unior

- Seperti namanya menggabungkan dua buah data berdasarkan *tabel yang sama*

## C. Agregate

- Fungsi yang melakukan perhitungan terhadap seluruh kolom pada suatu atribut
  - MIN (mencari nilai minimum)
  - MAX (mencari nilai max)
  - SUM (jumlah)
  - AVG (rata rata)
  - COUNT (hitung untuk syarat tertentu atau atribut tertentu)
  - HAVING (where versi agregasi)

## D. Function

- SQL terkadang juga menyediakan fungsi untuk mengolah data ketika akan dimasukan kedalam tabel, fungsi ini dibuat dengan deklarasi dan dipangil dengan trigger.
- `ReturnS DETERMINISTIC` memiliki arti bahwa yang dapat menggunakan fungsi hanya pembuat/root

```sql
DELIMITER $$
DEFINE FUNCTION `nama fungsi` (`parameter` dan `vaaraibel`)
BEGIN
  `Function body`
END$$
```

## E. Trigger Function

- Fungsi yang akan dijalankan ketika suatu operasi terjadi terhadap database

```sql
DELIMITER $$
DEFINE TRIGGER `nama trigger`
(BEFORE || AFTER) (DELETE || INSERT || UPDATE)
ON `nama tabel`
FOR EACH ROW
BEGIN
  `Trigger body`
END$$
```

## Tambahan

- Untuk menyimpan file, database hanya menyimpan nilai path terhadap server


# Praktikum

## 1. Insert

- [Script](./praktikum/script_1.sql)
- [Bukti run](./screenshots/)

### a. 5 operator
```sql
INSERT INTO operators(name) VALUES ('A'), ('B'), ('C'), ('D'), ('E'); 
```
### b. 3 product type
```sql
INSERT INTO product_types(name) VALUES ('T-A'), ('T-B'), ('T-C');
```
### c. 2 product, type id 1, operator id 3;
```sql
INSERT INTO products(product_type_id,operator_id,name,code,status) VALUES (1,3,'Ayam','AY01',1),(1,3,'Daging Sapi','DS01',1);
```
### d. 3 product, type id 2, operator id 1;
```sql
INSERT INTO products(product_type_id,operator_id,name,code,status) VALUES (2,1,'Aqua','AQ01',1),(2,1,'Ades','AD01',1),(2,1,'MIlo','MI01',1);
```
### e. 3 product, type id 3, operator id 4;
```sql
INSERT INTO products(product_type_id,operator_id,name,code,status) VALUES (3,4,'Apel','AP01',1),(3,4,'Jeruk','JE01',1),(3,4,'Pisang','PI01',1);
```
### f. product desc
```sql
INSERT INTO product_descriptions(product_id,description) VALUES (1,'Ayam kampung'),(2,'Sapi belanda'),(3,'Mineral danoe'),(4,'Mineral coca'),(5,'Minuman coklat'),(6,'Apel malang'),(7,'Jeruk mandarin'),(8,'Pisang tanduk');
```
### g. 3 payment
```sql
INSERT INTO payment_methods(id,name,status) VALUES (1,'transfer',1),(2,'cod',0),(3,'kredit',1);
```
### h. 5 user
```sql
INSERT INTO USERS(name,dob,status,gender) VALUES ('Daniel','01-09-01',1,'M'),('Bush','80-09-01',1,'M'),('Doe','90-09-01',1,'F'),('John','02-09-01',1,'F'),('Robert','2000-09-01',1,'M');
```
### i. 3 transaksi/user
```sql
INSERT INTO transaction(user_id,payment_method_id,status,total_qty,total_price)
  VALUES
    (1,1,'berhasil',3,6000),
    (1,2,'gagal',3,6000),
    (1,3,'menunggu',3,6000),
    (2,1,'berhasil',3,6000),
    (2,2,'gagal',3,6000),
    (2,3,'menunggu',3,6000),
    (3,1,'berhasil',3,6000),
    (3,2,'gagal',3,6000),
    (3,3,'menunggu',3,6000),
    (4,1,'berhasil',3,6000),
    (4,2,'gagal',3,6000),
    (4,3,'menunggu',3,6000),
    (5,1,'berhasil',3,6000),
    (5,2,'gagal',3,6000),
    (5,3,'menunggu',3,6000);

```
### j. 3 product per transaction
```sql
INSERT INTO transaction_details(transaction_id,product_id,status,price)
  VALUES
    (1,1,'tersedia',1000),
    ...
    (15,8,'tersedia',3000);
  ```

## 2. Select
### a. User laki laki
```sql
SELECT * FROM USERS WHERE GENDER = 'M';
```
### b. Product id 3
```sql
SELECT * FROM Products WHERE ID = 3;
```
### c. User created 7 hari belakang dan namanya terdapat huruf a
```sql
SELECT * FROM users WHERE timestampdiff(DAY,users.created_at,NOW())<= 7 AND (users.name LIKE '%a%' OR users.name LIKE '%A%'); 
```
### d. User Perempuan
```sql
SELECT * FROM USERS WHERE GENDER = 'F';
```
### e. Pelanggan abjad nama
```sql
SELECT * FROM USERS ORDER BY NAME;
```
### f. 5 data produk
```sql
SELECT * FROM products LIMIT 5; 
```

### 3. Update
### a. Ganti nama product dengan id 1
```sql
UPDATE products set nama = 'product dummy' where id = 1; 
```
### b. Ubah quantitiy produk dengan id 1 pada detail transaksi menjadi 3
```sql
UPDATE transaction_details set quantity = 3 where product_id = 1;
```

### 4. Delete
### a. Hapus product dengan id 1
```sql
delete from products where id = 1; 
```
### b. Hapus product dengan type id 1
```sql
delete from products where products.product_type_id = 1; 
```

## Part 2
### 1. Gabungan transaksi user id 1 dan 2
```sql
SELECT * FROM TRANSACTION WHERE user_id = 1 UNION SELECT * FROM transaction WHERE user_id = 2; 
```
### 2. Transaksi total user id 1
```sql
SELECT SUM(TRANSACTION.total_price) FROM TRANSACTION WHERE USER_ID = 1;
```
### 3. Total transaksi dengan product type 2
```sql
SELECT count(transaction_details.transaction_id) FROM transaction_details LEFT JOIN products ON transaction_details.product_id = products.id WHERE products.product_type_id=2; 
```
### 4. Fielt product dan nama type-nya
```sql
SELECT pt.name,P.* FROM products as P LEFT JOIN product_types as PT ON P.product_type_id = PT.id;
``` 
### 5. Semua field transaksi, field nama produk, field nama user
```sql
select UT.*,PD.Product from (SELECT t.*,u.NAMA as `Nama User` from transaction as t left join users as u on t.user_id = u.id) as UT ,(SELECT td.transaction_id as pid, p.nama as Product FROM transaction_details as td, products as p WHERE td.product_id = p.id) as PD where PD.pid = UT.id order by UT.id; 
```
### 6. Function setelah data transaksi dihapus maka detail transaction juga terhapus

```sql
DELIMITER $$
CREATE OR REPLACE FUNCTION delTransaction(tid int)
RETURNS int
  BEGIN
    DELETE FROM transaction_details WHERE transaction_id = tid;
    DELETE FROM transaction WHERE id = tid;
    RETURN 0;
  END$$
```
### 7. Function setelah delete detail transaksi, maka pada transaksi, total barang akan berkurang sesuai dengan barang pada detail transaksi yang dihapus
```sql
DELIMITER $$  

CREATE OR REPLACE TRIGGER red_trans_qty
  BEFORE DELETE
  ON transaction_details
  FOR EACH ROW  
  BEGIN
    UPDATE transaction SET total_qty = (total_qty - old.quantity) WHERE id = old.transaction_id;
  END$$
```
### 8. Produk yang tidak ada di detail transaksi dengan subquery
```sql
SELECT * FROM products where id NOT IN (SELECT product_id FROM transaction_details);
```
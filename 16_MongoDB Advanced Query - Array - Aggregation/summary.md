# (16) MongoDB Advanced Query - Array - Aggregation

# Summary

## Import export DB

- hal ini dapat diwujudkan dengan mengguanakn `Monodb database tools`


## Advance Query

### Comparison

- $ne not equals
- $ge greater
- $gte greater than
- $le less
- $lte less than

### Logical

- $or, $and
- $in, $nin
- $not

### Evaluator

- $regex

### Summary

![hasil](./screenshots/sum.png)

# Praktikum

```js
db.books.insertMany([
  {
    _id:1,
    title:'Wawasan Pancasila',
    authorId:1,
    publisherId:1,
    price:71200,
    stats:{
      page:324,
      weight:300,
    },
    publishedAt: new Date('2018-10-01'),
    category: [
      'social',
      'politics',
    ]
  },
  {
    _id:2,
    title:'Mata Air Keteladanan',
    authorId:1,
    publisherId:2,
    price:106250,
    stats:{
      page:672,
      weight:650,
    },
    publishedAt: new Date('2017-09-01'),
    category: [
      'social',
      'politics',
    ]
  },
  {
    _id:3,
    title:'Revolusi Pancasila',
    authorId:1,
    publisherId:1,
    price:54400,
    stats:{
      page:220,
      weight:500,
    },
    publishedAt: new Date('2015-05-01'),
    category: [
      'social',
      'politics',
    ]
  },
  {
    _id:4,
    title:'Self Driving',
    authorId:2,
    publisherId:1,
    price:58650,
    stats:{
      page:286,
      weight:300,
    },
    publishedAt: new Date('2018-05-01'),
    category: [
      'self-development',
    ]
  },
  {
    _id:5,
    title:'Self Disruption',
    authorId:2,
    publisherId:2,
    price:83300,
    stats:{
      page:400,
      weight:800,
    },
    publishedAt: new Date('2018-05-01'),
    category: [
      'self-development',
    ]
  },
]);

db.authors.insertMany([
  {
    _id:1,
    firstName:'Yudi',
    lastName:'Latif',
  },
  {
    _id:2,
    firstName:'Rheinald',
    lastName:'Kasali',
  },
]);

db.publishers.insertMany([
  {
    _id:1,
    publisherName:'Expose',
  },
  {
    _id:2,
    publisherName:'Mizan',
  },
]);
```

# Praktikum

[hasil](./screenshots)

## 1. Data buku author id 1 dan 2 digabung.

```js
db.books.find({$or:[{authorId:{$eq:1}},{authorId:{$eq:2}}]});
```
## 2. Daftar buku & harga author id 1

```js
db.books.find({authorId:1},{_id:1,title:1,price:1});
```

## 3. total halama author id 2
```js
db.books.aggregate([
  {
    $match: {authorId:2}
  },
  {
    $group: {
      _id: '$authorId',
      totalPage: {$sum:'$stats.page'}
    }
  }]);
```

## 4. Author dan buku terkait
```js
db.authors.aggregate([{
  $lookup: {
    from: 'books',
    localField: '_id',
    foreignField: 'authorId',
    as: 'books'
  }
}]);

db.books.aggregate([{
  $lookup: {
    from: 'authors',
    localField: 'authorId',
    foreignField: '_id',
    as: 'author'
  }
}]);
```

## 5. Books -> author -> publisher

```js
db.books.aggregate([
  {$lookup: 
  {
    from: 'authors',
    localField: 'authorId',
    foreignField: '_id',
    as: 'author'
  }},
  {$lookup:
  {
	  from: 'publishers',
	  localField: 'publisherId',
	  foreignField: '_id',
	  as: 'publisher'
  }},
]);
```
## 6. author -> books number -> publisher & book's name output

```js
db.authors.aggregate([
  {
    $lookup: {
      from: 'books',
      localField: '_id',
      foreignField: 'authorId',
      as: 'books'
    }
  },
  {
    $unwind: {
      path: '$books',
      preserveNullAndEmptyArrays: true
    }
  },
  {
    $lookup: {
      from: 'publishers',
      localField: 'books.publisherId',
      foreignField: '_id',
      as: 'publisher'
    }
  },
  {
    $group: {
      _id: { $concat: [ '$firstName',' ','$lastName' ] },
      number_of_books: { "$sum": 1 },
      list_of_book: { $push: { $concat: [ '$books.title',' (',{ $arrayElemAt: [ '$publisher.publisherName', 0 ] },')' ] } }
    } 
  },
  {
    $sort: { number_of_books : 1 } 
  }
]);
```

## 7. Diskon buku berdasarkan harga
```js
db.books.aggregate([
  {
    $project:{
      _id:1,
      title:1,
      price:1,
      Promo:{
        $switch: {
          branches: [
            {
              case: { $lt: [ '$price', 60000 ] },
              then: '1%'
            },
            {
              case: { $lt: [ '$price', 90000 ] },
              then: '2%'
            },
            {
              case: { $gte: [ '$price', 90000 ] },
              then: '3%'
            },
          ],
          default: "N/A."
        }
      }
    }
  }
]);
```

## 8. Nama buku, harga, penulis,penerbit urut harga

```js
db.books.aggregate([
  {
    $lookup: {
      from: 'authors',
      localField: 'authorId',
      foreignField: '_id',
      as: 'author'
    }
  },
  {
    $lookup: {
      from: 'publishers',
      localField: 'publisherId',
      foreignField: '_id',
      as: 'publisher'
    }
  },
  {
    $project: {
      _id: 0,
      title: '$title',
      price: '$price',
      author: { $concat: [ { $arrayElemAt: [ '$author.firstName', 0 ] },' ',{ $arrayElemAt: [ '$author.lastName', 0 ] } ] },
      publisher: { $arrayElemAt: [ '$publisher.publisherName', 0 ] }
    } 
  },
  {
    $sort: {
      price : -1
    }
  }
]);
```

## 9. Tampilkan Buku dan detainya, lengkap, kemudian hanya id 3 dan 4

```js
db.books.aggregate([
  {
    $lookup: {
      from: 'publishers',
      localField: 'publisherId',
      foreignField: '_id',
      as: 'publisher'
    }
  },
  {
    $project: {
      title: '$title',
      price: '$price',
      publisher: { $arrayElemAt: [ '$publisher.publisherName', 0 ] }
    } 
  },
  {
    $skip: 2
  },
  {
    $limit: 2
  }
]);
```
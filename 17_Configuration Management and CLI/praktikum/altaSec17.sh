#!/bin/sh

# Menyimpan argumen nama, id facebook, id linkedin
var_name=$1
var_fb=$2
var_lin=$3

# Menambil nilai tanggal sistem
var_date=$(date)

# Efektif nama direktori
var_rootDir="$1 at $var_date"

# Menyimpan data ping
var_ping=$(ping -c 3 google.com)

mkdir "$var_rootDir"

# Alamat facebook
mkdir -p "$var_rootDir"/about_me/personal && echo "https://www.facebook.com/$2" >> "$var_rootDir"/about_me/personal/facebook.txt

# Alamat linkedin
mkdir -p "$var_rootDir"/about_me/professional && echo "https://www.linkedin.com/in/$3" >> "$var_rootDir"/about_me/professional/linkedin.txt

# Data pertemanana
mkdir -p "$var_rootDir"/my_friends && curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt -o "$var_rootDir"/my_friends/list_of_my_friends.txt

# Data informasi sistem dan menyimpan ping
mkdir -p "$var_rootDir"/my_system_info && echo $var_ping >> "$var_rootDir"/my_system_info/internet_connection.txt
echo $'My username: '$(whoami)$'\nWith host: '$(uname -a) >> "$var_rootDir"/my_system_info/about_this_laptop.txt
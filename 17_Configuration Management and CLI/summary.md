# (17) Configuration Management and CLI

- [Summary](#summary)
- [Praktikum](#praktikum)

# Praktikum

## A. Perkenalan

|GUI|CLI|
|---|---|
|Drag and Drop|Keyboard only|

- CLI memiliki kelebihan karna kecepatan dan ketepatan
- Menawarkan interaksi yang lebih dibandingkan mengguankan GUI
- Manajemen lebih cepat menggunakan CLI
- `Granula Access` dalam mengendalikan OS kita dapat berubah identitas CLI
- Bisa membuat & menyimpan `script` guna otomasi pekerjaan yang rutin
- Bisa sebagai tempat untuk mengetahui status suatu proses

### CLI

- OS's Shell :
  - UNIX Shell
  - Command Prompt(MSDOS)

- Non-OS :
  - MySQL CLI client
  - redis-cli
  - Mongo Cli

![sum](./screenshots/sum1.png)

|Root|Normal|Sudoers|
|---|---|---|
|Semua direktori|Manipulasi direktori user itu sendiri|Akses Root Sementara|

## B. Perintah Direktori

|Perintah|Deskripsi|
|---|---|
|`pwd`|untuk mengetahui lokasi direktori terakhir|
|`cd`|berpindah direktori|
|`ls`|menampilkan isi direktori terakhir|
|`mkdir`|membuat direktori baru|
|`rm`|menghapus file atau direktori (jika berisi gunakan argumen -r)|
|`mv`|mengganti/memindahkan file|
|`ln`|Membuat link antar file (soft shortcut(reference), hard data pada inode(sturktur data) langsung)|

### Izin dari suatu file
![sum](./screenshots/sum2.png)
![sum](./screenshots/sum3.png)

## C. Perintah Dokumen

|Peintah|Deskirpsi|
|---|---|
|`touch`|membuat dokumen baru|
|`cat`;`head`;`tail`|membuka file|
|`vim`;`nano`|editor dari dokumen|
|`chown`;`chmod`|mengubah kepemilikan;mengubah akses|
|`diff`|melihat perbedaan konten data file|

## D. Perintah Jaringan

|Peintah|Deskirpsi|
|---|---|
|`ping`|menguji response koneksi dari alamat tujuan|
|`wget`|mengunduh file arsip yang tersedia pada link|
|`ssh`|protokol kredensial|
|`curl`|mendapatkan data dari link|
|`netsat`|merinci penggunaan komunikasi lokal|
|`nmap`|merinci penggunaan port|
|`ifconfig`|mendeskripsikan informasi jaringan mesil lokal|

## E. Perinah Utilitas

|Peintah|Deskirpsi|
|---|---|
|`mau`|menampilkan panduan|
|`env`|penetapan lingkungan pengembangan|
|`echo`|mengembalikan string|
|`date`|mengembalikan string waktu|
|`which`|lokasi file|
|`watch`|monitor proses yang berjalan|
|`sudo`|keredensial sementara|
|`history`|log perintah|
|`grep`|mencari file yang mengandung argumen|
|`lcoate`|mencari file|

## F. File SH

```sh
#!/bin/sh
# perintah
```
- merupakan bahasa pemrograman
- berfungsi sebagai automasi
- dalam definisi variable case sensitive (space juga)
- mengakeses data dengan `$`, hal ini juga bisa diartikan sebagai literan `CLI` karna kita bisa melakukan operasi juga dengan menambahkan tanda kurung setelah tanda peso.
- selain itu didalam file scirpt ini kita juga bisa menjalankan seluruh perintah yang biasa dijalankan di `CLI`
```sh
# definisi variabel
namaVar={string/angka/dls}
```
```sh
# operasi variabel, mendapatkan nama user
namaVar=$(whoami) 
```
- Lakukan perubahan izin agar program bisa tereksekusi 
```
chmod -x script.sh
```
# Praktikum

https://user-images.githubusercontent.com/74223938/159151781-210eac3c-a09f-4018-a0ca-2516fe931697.mp4

## 1. Terinstall Oh My Zsh

- Bukti terinstall bisa dilihat divideo, namun belum mengatur theme

## 2. Tugas Script
```sh
#!/bin/sh

# Menyimpan argumen nama, id facebook, id linkedin
var_name=$1
var_fb=$2
var_lin=$3

# Menambil nilai tanggal sistem
var_date=$(date)

# Efektif nama direktori
var_rootDir="$1 at $var_date"

# Menyimpan data ping
var_ping=$(ping -c 3 google.com)

mkdir "$var_rootDir"

# Alamat facebook
mkdir -p "$var_rootDir"/about_me/personal && echo "https://www.facebook.com/$2" >> "$var_rootDir"/about_me/personal/facebook.txt

# Alamat linkedin
mkdir -p "$var_rootDir"/about_me/professional && echo "https://www.linkedin.com/in/$3" >> "$var_rootDir"/about_me/professional/linkedin.txt

# Data pertemanan
mkdir -p "$var_rootDir"/my_friends && curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt -o "$var_rootDir"/my_friends/list_of_my_friends.txt

# Data informasi sistem dan menyimpan ping

# Dilakukan operasi print dengan single quote agar echo bisa mengubah `\n` menjadi whitespace

#whoami untuk mendapatkan nama user
#uname -a mendapatkan deskripsi sistem

mkdir -p "$var_rootDir"/my_system_info && echo $var_ping >> "$var_rootDir"/my_system_info/internet_connection.txt
echo $'My username: '$(whoami)$'\nWith host: '$(uname -a) >> "$var_rootDir"/my_system_info/about_this_laptop.txt
```


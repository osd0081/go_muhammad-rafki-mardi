# (18) System Design

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. Diagram

- dalam membangun sistem pembuatan diagram sangat penting guna visualisasi alur
- Diagram umum:

|Jenis|Deskripsi|
|---|---|
|Flowchart|Alur logika dari proses|
|Use case|Kegiatan antara penguna dengan sistem|
|ERD|Representasi relasi entitas data |
|HLA|Gambaran umun dari arsitektur sistem(bahasa pemrograman)|

## B. Distributed System

- Jika system sudah mempunyai workload yang besar maka harus dilakukan pemecahan sistem

|Karakteristik|Deskrisi|
|---|---|
|Scalability|vertical scale, meningkatkan kemampuan komputasi. Horisontal, kemampuan penyimpanan|
|Reliability|kemungkinan gagal sistem dalam periode tertentu|
|Availability|kemampuan untuk melaksanakan tugas sesuai dengan waktu spesifik|
|Efficiency|minim latensi dan laju throughput|
|Servicability or Manageability|waktu sistem mengalami perbaikan atau perawatan, hal ini bisa didapat dengan mengetahui kapan terjadinya kegagalan, perubahan dan perkembangan

## C. Job Queue & Microservice
  
### Job/Work Queue

- Queue, merupakan teknik memasukan perkerjaan sistem kedalam kesebuah antrian dan melepas pengguna sehinga pengguna tidka perlu menunggu hingga sistem selesai

### Load balancing

- distribusi trafik sistem sehingga beban kerja terdistribusi
  - sistem dengan server
  - web server dan layer dalam
  - antara layer platform dengan database

### Monolithic

- model sistem dengan satu server (base code) dan multi module (fitur). dan memiliki file eksekutor

### Microservices

- model sistem arsitektur terpisah pisah baik dengan container, sehingga kemampuan serta fleksibilitas pengembangan tinggi
  
## D. SQL & no-SQL

- penggunaan SQL dan no-SQL harus menyesuaikan proses sistem guna mecapai prinsip yang effisien

## E. Caching

- ketika sebuah data sering diminta, maka layannya kita tidak memproses data tersebut kembali, namun disimpan kedalam cache sehingga beban kerja dari sistem berkurang.
- keberadaan cache bisa pada setiap tingkat sistem namun baiknya hanya pada tingkat antarmuka
- penyimpanan cache bisa dlakukan di ram

## F. Replikasi Database

- baiknya kita menyimpan database terdisitribusi agar tidak terjadi SPF.

## G. Indexing

- Dalam melakukan akses database harus dilakukan operasi yang efektif agar mencapai waktu yang efisien, hal ini bisa dilakukan dengan database indexing dan membuat view.

## F. Repliasi database

# Praktikum

## Problem 1

### 1. ERD

![hasil](./praktikum/erd-alta_sec_18.png)

### 2. USE CASE

![hasil](./praktikum/uc-alta_sec_18.png)

## Problem 2

### redis

```
keys *users*
```

### Neo4J(Cypher)

```
MATCH (p:user)
RETURN p;
```

### Cassandra (3.0)

```
select * from users
```
# (19) Intro RESTful API

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. API 


- `API` adalah application programming interface yang merupakan kumpulan fungsi atau prosedur untuk mengakses aplikasi tersebut untuk mendapatkan data data yang dibutuhkan

![hasil](./screenshots/sum1.png)

## B. RESTful

- `RE`presentational `S`tate `T`ransfer, menggunakan HTTP Protocol

- Fungsi:
  - Untuk integrasi aplikasi antara frontend dengan backend
  - Mewujudkan multi language application

### Integrasi Backend dengan Backend

- Kita dapat memecah service sehingga terdistribusi dan dihubungkan dengan API

### JSON

- Merupakan `javascript object notation`

### Response Code

|Code|Deskripsi|
|---|---|
|200|Ok|
|201|Created|
|400|Bad Request|
|401|Unauthorized|
|404|Not Found|
|405|Method Not Allowed|
|500|Internal Server Error|

## C. API Tools

- Macam:
  - Postman
  - SoapUI
  - JMeter
  - Katalon

### Postman

- Merupakan aplikasi testing backend untuk menguji aplikasi backend

### Best Practice

- Menggunakan kata benda
- Menggunakan kata jamak
- Lakukan resource nesting yang tepat
- Format Response `JSON`:
  - Header
  - Meta
  - Data
  - Pagination
- Kita dapat melakukan kegiatan `filtering` `sorting` `paging` dan `field selection`
- Mampu mengahandler `trailing slashes`
- Melakukan _versioning_

## D. Implementasi

- untuk membuat server api dibutuhkan package berikut:
  - net/http guna melakukan traffic
  - encoding/json melakukan pengolahan pengolahan data json

# Praktikum

## 1. Newsapi

### Get All News With Keyword Apple

```
GET https://newsapi.org/v2/top-headlines?country=id&apiKey={{apiKey}}
```

![hasil](./screenshots/1-b.png)

### Get All News With Headline In Indonesian

```
GET https://newsapi.org/v2/everything?q=bitcoin&sortBy=popularity&pageSize=5&apiKey={{apiKey}}
```

![hasil](./screenshots/1-c.png)

### Get Top 5 News With Keyword BITCOIN

```
GET https://newsapi.org/v2/everything?q=bitcoin&sortBy=popularity&pageSize=5&apiKey={{apiKey}}
```

![hasil](./screenshots/1-c.png)

### Get Popular News With Keyword China Airlines From 20 March 2022 to 22 March 2022

```
GET https://newsapi.org/v2/everything?q="China Airlines"&from=2022-03-22&to=2022-03-22&sortBy=popularity&apiKey={{apiKey}}
```

![hasil](./screenshots/1-d.png)

### Get All News With Title Keywords Mandalika in English

```
GET https://newsapi.org/v2/everything?q="Mandalika"&language=en&searchIn=title&sortBy=relevancy&apiKey={{apiKey}}
```

![hasil](./screenshots/1-e.png)

## 2. swapi

### Get All Person with Name Darth

```
https://swapi.dev/api/people?search=darth
```

![hasil](./screenshots/2-a.png)

### Get All Planet with Name Contains "ta"

```
GET https://swapi.dev/api/planets?search=ta
```

![hasil](./screenshots/2-b.png)

### Get All Films with TItle Contains "sith"

```
GET https://swapi.dev/api/films?search=sith
```

![hasil](./screenshots/2-c.png)

### Get All Starships with Model Corvette

```
GET https://swapi.dev/api/starships?search=corvette
```

![hasil](./screenshots/2-d.png)

### Get Species with id 10

```
GET https://swapi.dev/api/species/10
```

![hasil](./screenshots/2-e.png)

## 3. sepulsa

### Get Book by Id

```
GET https://virtserver.swaggerhub.com/sepulsa/RentABook-API/1.0.0/book/{{bookId}}
```

![hasil](./screenshots/3-a.png)

### Add New Book

```
POST https://virtserver.swaggerhub.com/sepulsa/RentABook-API/1.0.0/book
```

![hasil](./screenshots/3-b.png)

### Update Book by Id

```
PUT https://virtserver.swaggerhub.com/sepulsa/RentABook-API/1.0.0/book/{{bookId}}
```

![hasil](./screenshots/3-c.png)

### Delete Book by Id

```
DELETE https://virtserver.swaggerhub.com/sepulsa/RentABook-API/1.0.0/book/{{bookId}}
```

![hasil](./screenshots/3-d.png)
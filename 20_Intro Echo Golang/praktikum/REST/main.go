package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

type User struct {
	Id       int    `json:"id" form:"id"`
	Name     string `json:"name" form:"name"`
	Email    string `json:"email" form:"email"`
	Password string `json:"password" form:"password"`
}

var users []User

func GetUsersController(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "success get all users",
		"users":   users,
	})
}

func GetUserByIdController(c echo.Context) error {
	userId, _ := strconv.Atoi(c.Param("id"))
	var user User
	for _, el := range users {
		if el.Id == userId {
			user = el
		}
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"code": "200",
		"data": user,
	})
}

func DeleteUserByIdController(c echo.Context) error {
	userId, _ := strconv.Atoi(c.Param("id"))
	for in, el := range users {
		if el.Id == userId {
			users = append(users[:in], users[in+1:]...)
		}
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"code":    "200",
		"message": "success delete user",
	})
}

func UpdateUserByIdController(c echo.Context) error {
	user := new(User)
	if err := c.Bind(&user); err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, map[string]interface{}{
			"code":    "500",
			"message": "server error",
		})
	}
	userId, _ := strconv.Atoi(c.Param("id"))
	for in, el := range users {
		if el.Id == userId {
			users[in].Name = user.Name
			users[in].Email = user.Email
			users[in].Password = user.Password
		}
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"code":    "200",
		"message": "success update user",
	})
}

func CreateUserController(c echo.Context) error {
	user := User{}
	if err := c.Bind(&user); err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, map[string]interface{}{
			"code":    "500",
			"message": "server error",
		})
	}
	fmt.Println(user)
	if len(users) == 0 {
		user.Id = 1
	} else {
		newId := users[len(users)-1].Id + 1
		user.Id = newId
	}
	users = append(users, user)
	return c.JSON(http.StatusOK, map[string]interface{}{
		"code":    "201",
		"message": "success create user",
	})
}

func main() {
	e := echo.New()
	e.POST("/users", CreateUserController)
	e.GET("/users", GetUsersController)
	e.GET("/users/:id", GetUserByIdController)
	e.DELETE("/users/:id", DeleteUserByIdController)
	e.PUT("/users/:id", UpdateUserByIdController)
	e.Logger.Fatal(e.Start(":8080"))
}

# (20) Intro Echo Golang

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Third Party Library

- Library eksternal yang dapat diakses program golang kita.

## Echo

### Introduction

```go
go get github.com/labstack/echo/v4
```

- Jangan lupa melakukan import dependensi
- Sesuaikan import dengan list dependensi yang berada di `go.mod`
- Golang echo adalah third party library yang bersifat ekstensibel dan minimalis guna membantu dalam pengembangan restAPI.
- Keuntungan:
  - Optimized Router
  - Middleware
  - Data Rendering
  - Scalable
  - Data Binding
  - Tanpa struktur
- Untuk database dan ORM, echo belum mendukung kegiatan tersebut.
  - Database GROM
  - Autentikasi JWT

### Hands On Route Method
- data `echo.New()` memiliki method untuk melakukan operasi API

### Hands On echo Contex
- `echo.Context` memegang data yang dikirim ke API kita, dengann ya kita bisa unpacking dengan
  - QueryParam
  - Param
  - FormValue

### Hands On echo JSON Data
- Binding data terhadap struct dengan menambahkan property tambahan

```go
 In int `json: "id" form: "id"`
```


# Praktikum

## Tambahan 

Menggunaakan pengecekan error dengan hal berikut

```
if err := c.Bind(&user); err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, map[string]interface{}{
			"code":    "500",
			"message": "server error",
		})
	}
```

## Post User

![hasil](./screenshots/post.png)

## Get all users data

![hasil](./screenshots/getAll.png)

## Get User By Id

![hasil](./screenshots/getById.png)

## Delete User By Id

![before](./screenshots/deleteById-Before.png)
![after](./screenshots/deleteById-After.png)

## Update User By Id

![before](./screenshots/puById-Before.png)
![after](./screenshots/puById-After.png)
package main

import (
	"fmt"
	"net/http"
	"github.com/labstack/echo/v4"
	"github.com/jinzhu/gorm"
  _"github.com/jinzhu/gorm/dialects/mysql"
	"errors"
	"strconv"
)

var (
	DB *gorm.DB
)

func init() {
	InitDB()
	InitialMigration()
}

type Config struct {
	DB_Username string
	DB_Password string
	DB_Port	string
	DB_Host	string
	DB_Name string
}

func InitDB() {

	config := Config{
		DB_Username: "root",
		DB_Password: "",
		DB_Host: "3306",
		DB_Port: "localhost",
		DB_Name: "alta_section_21",
	}

	connectionString := 
		fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		config.DB_Username,
		config.DB_Password,
		config.DB_Port,
		config.DB_Host,
		config.DB_Name,
	)

	var err error
	DB, err = gorm.Open("mysql", connectionString)
	if err != nil {
		panic(err)
	}
}

type User struct {
	gorm.Model
	Name string `json:"name" form:"name" required:"name"`
	Email string `json:"email" form:"email" required:"email"`
	Password string `json:"password" form:"password" required:"password"`
}

func InitialMigration() {
	DB.AutoMigrate(&User{})
}

func GetUsersController (c echo.Context) error {
	var users []User
	
	if err := DB.Find(&users).Error; err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"message":"success get all users",
		"users": users,
	})
}

func GetUserController (c echo.Context) error {
	var user User
	id := c.Param("id")
	
	if result := DB.First(&user, id); result.Error != nil {
		return echo.NewHTTPError(http.StatusBadRequest, errors.Is(result.Error, gorm.ErrRecordNotFound))
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"nessage" : "success retriving user",
		"user": user,
	})	
}

func CreateUserController (c echo.Context) error {
	user := User{}
	c.Bind(&user)
	 
	if err := DB.Save(&user).Error; err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	
	return c.JSON(http.StatusOK, map[string]interface{}{
		"nessage" : "success create new user",
		"user": user,
	})
}

func DeleteUserController (c echo.Context) error {
	var user User
	id := c.Param("id")
	
	if result := DB.Delete(&user, id); result.Error != nil {
		return echo.NewHTTPError(http.StatusBadRequest, errors.Is(result.Error, gorm.ErrRecordNotFound))
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"nessage" : "user deleted",
	})
}

func UpdateUserController (c echo.Context) error {
	var user User
	id, _:= strconv.Atoi(c.Param("id"))

	userUpdate := User{}
	c.Bind(&userUpdate)

	DB.First(&user, id)
	
	if result := DB.Model(&user).Where("id=?",id).Updates(&userUpdate); result == nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Update gagal")
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"nessage" : "user updated",
		"user": user,
	})
}

func main() {
	e := echo.New()

	e.GET("/users",GetUsersController)
	e.GET("/users/:id",GetUserController)
	e.POST("/users",CreateUserController)
	e.DELETE("/users/:id",DeleteUserController)
	e.PUT("/users/:id",UpdateUserController)

	e.Logger.Fatal(e.Start(":8000"))
}
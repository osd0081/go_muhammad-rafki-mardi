package controllers

import (
	"net/http"
	"github.com/labstack/echo/v4"
	"problem2/lib/database"
	"problem2/models"
	"strconv"
)

func GetBooksController(c echo.Context) error {
	books, e := database.GetBooks()
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"books": books,
	})
}

func GetBookController(c echo.Context) error {
	id, _:= strconv.Atoi(c.Param("id"))
	book, e := database.GetBookById(id)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"books": book,
	})
}

func CreateBookController(c echo.Context) error {
	var book models.Book
	c.Bind(&book)
	result, e := database.CreateBook(book)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"books": result,
	})
}

func DeleteBookController (c echo.Context) error {
	id, _:= strconv.Atoi(c.Param("id"))
	result, e := database.DeleteBookById(id)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"books": result,
	})
}

func UpdateBookController (c echo.Context) error {
	var book models.Book
	id, _:= strconv.Atoi(c.Param("id"))
	c.Bind(&book)
	result, e := database.UpdateBookById(id,book)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"books": result,
	})
}
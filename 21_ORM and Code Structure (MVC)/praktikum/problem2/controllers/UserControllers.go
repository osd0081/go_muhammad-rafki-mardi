package controllers

import (
	"net/http"
	"github.com/labstack/echo/v4"
	"problem2/lib/database"
	"problem2/models"
	"strconv"
)

func GetUsersController(c echo.Context) error {
	users, e := database.GetUsers()
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": users,
	})
}

func GetUserController (c echo.Context) error {
	id, _:= strconv.Atoi(c.Param("id"))
	user, e := database.GetUserById(id)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": user,
	})
}

func CreateUserController (c echo.Context) error {
	var user models.User
	c.Bind(&user)
	result, e := database.CreateUser(user)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": result,
	})
}

func DeleteUserController (c echo.Context) error {
	id, _:= strconv.Atoi(c.Param("id"))
	result, e := database.DeleteUserById(id)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": result,
	})
}

func UpdateUserController (c echo.Context) error {
	var user models.User
	id, _:= strconv.Atoi(c.Param("id"))
	c.Bind(&user)
	result, e := database.UpdateUserById(id,user)
	if e != nil {
		return echo.NewHTTPError(http.StatusBadRequest, e.Error())
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": result,
	})
}
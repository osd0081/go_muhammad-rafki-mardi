package database

import (
	"problem2/config"
	"problem2/models"
)

func GetBooks() (interface{}, error) {
	var books []models.Book
	if e := config.DB.Find(&books).Error; e != nil {
		return nil, e
	}
	return books, nil
}

func GetBookById(id int) (interface{}, error) {
	var book models.Book
	if e := config.DB.Find(&book,id).Error; e != nil {
		return nil, e
	}
	return book, nil
}

func CreateBook(bookData models.Book) (interface{}, error) {
	if e := config.DB.Save(&bookData).Error; e != nil {
		return nil, e 
	}
	return bookData, nil
}

func UpdateBookById(id int, bookData models.Book) (interface{}, error) {
	var book models.Book
	
	config.DB.First(&book, id)
	var e error
	if result := config.DB.Model(&book).Where("id=?",id).Updates(&bookData); result == nil {
		return nil, e
	}
	return book, nil
}

func DeleteBookById(id int)(interface{}, error){
	var book models.Book
	var e error
	if result := config.DB.Delete(&book, id); result == nil {
		return nil, e
	}
	return "book deleted", nil
}
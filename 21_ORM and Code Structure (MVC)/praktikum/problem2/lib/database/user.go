package database

import (
	"problem2/config"
	"problem2/models"
)

func GetUsers() (interface{}, error) {
	var users []models.User
	if e := config.DB.Find(&users).Error; e != nil {
		return nil, e
	}
	return users, nil
}

func GetUserById(id int) (interface{}, error) {
	var user models.User
	if e := config.DB.Find(&user,id).Error; e != nil {
		return nil, e
	}
	return user, nil
}

func CreateUser(userData models.User) (interface{}, error) {
	if e := config.DB.Save(&userData).Error; e != nil {
		return nil, e 
	}
	return userData, nil
}

func UpdateUserById(id int, userData models.User) (interface{}, error) {
	var user models.User
	
	config.DB.First(&user, id)
	var e error
	if result := config.DB.Model(&user).Where("id=?",id).Updates(&userData); result == nil {
		return nil, e
	}
	return user, nil
}

func DeleteUserById(id int)(interface{}, error){
	var user models.User
	var e error
	if result := config.DB.Delete(&user, id); result == nil {
		return nil, e
	}
	return "user deleted", nil
}
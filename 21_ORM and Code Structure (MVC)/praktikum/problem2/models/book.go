package models

import (
	"github.com/jinzhu/gorm"
)

type Book struct {
	gorm.Model
	Title  string `json:"title" form:"title" required:"title"`
	Author string `json:"author" form:"author" required:"author"`
	Year   int  `json:"year" form:"year" required:"year"`
}

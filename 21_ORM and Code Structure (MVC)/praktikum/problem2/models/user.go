package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Name     string `json:"name" form:"name" required:"name"`
	Email    string `json:"email" form:"email" required:"email"`
	Password string `json:"password" form:"password" required:"password"`
}

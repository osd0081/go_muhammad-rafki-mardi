# (21) ORM and Code Structure (MVC)

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. ORM

- _Object Relational Mapping_ konversi model data pada database menjadi tipe data struct tertentu
- Keuntungan:
  - Mengurangi kueri yang repetitif
  - Data langsung menjadi struct
  - Validasi data
  - Fitur cache query
- Kelemahan:
  - Menggunakan & ketergantungan library pihak ketiga
  - Ketika join rentan masalah

## B. GORM

```go
<username>:<password>@/<db_name>?charset=utf8&parseTime=Ture&loc=Lock
```

### Instalasi

- Via [link berkut](grom.io)

### Migration

- Cara membuat dan memperbarui struktur database
- Ketika kesalahan struktur kita dapat melakukan _undo_
- Untuk tracking dan loging perubahan struktur database
- Grom melakukan migration dengan memasukan struct menjadi tabel database

### Controller

|Sintaks|Fungsi|
|---|---|
|DB.Find(*struct)|Melakukan query select terhadap tabel|
|DB.Save(*struct|Melakukan query insert terhadap tabel|

## C. Refactoring

- Model di paling atas bedara di `go.mod`
- Menggunakan fungsi package lain harus menyertakan nama package.
- Fungsi/tipe data yang namany berawalan dengan huruf kapital merupakan global function

### MVC

|Istilah|Analogi|
|---|---|
|Model|Struct|
|View|Tampilan|
|Controller|Pengolahan|


# Praktikum

## Problem 1

membangung API sederhana dengan bantuan GORM dan Echo

### Create User

![hasil](./screenshots/problem1/post.png)

### Get All Users

![hasil](./screenshots/problem1/getAllUser.png)

### Get User by Id

![hasil](./screenshots/problem1/getUserById.png)

### Update User by Id

![hasil](./screenshots/problem1/updateUserById1.png)
![hasil](./screenshots/problem1/updateUserById2.png)

### Delete User by Id

![hasil](./screenshots/problem1/deleteUserById1.png)
![hasil](./screenshots/problem1/deleteUserById2.png)

## Problem 2

[Video live demo program](https://drive.google.com/file/d/1lGZUZJ10uu6itjpmAEp-iUUnlY23BjS2/view?usp=sharing)

membangung API berstruktur MVC/Layered dengan bantuan GORM dan Echo

### Create User

![hasil](./screenshots/problem2/userCreate.png)

### Get All Users

![hasil](./screenshots/problem2/userGetAll.png)

### Get User by Id

![hasil](./screenshots/problem2/userGetById.png)

### Update User by Id

![hasil](./screenshots/problem2/userUpdateById.png)
![hasil](./screenshots/problem2/userUpdateByIdProof.png)

### Delete User by Id

![hasil](./screenshots/problem2/userDeleteById.png)
![hasil](./screenshots/problem2/userDeleteByIdProof.png)

### Create Book

![hasil](./screenshots/problem2/bookCreate.png)

### Get All Books

![hasil](./screenshots/problem2/bookGetAll.png)

### Get Book by Id

![hasil](./screenshots/problem2/bookGetById.png)

### Update Book by Id

![hasil](./screenshots/problem2/bookUpdateById.png)
![hasil](./screenshots/problem2/bookUpdateByIdProof.png)

### Delete Book by Id

![hasil](./screenshots/problem2/bookDeleteById.png)
![hasil](./screenshots/problem2/bookDeleteByIdProof.png)

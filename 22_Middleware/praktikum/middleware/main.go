package main

import (
	"problem2/config"
	m "problem2/middlewares"
	"problem2/routes"
)

func main() {
	config.InitDB()
	e := routes.New()
	m.LogMiddlewares(e)
	e.Logger.Fatal(e.Start(":8000"))
}

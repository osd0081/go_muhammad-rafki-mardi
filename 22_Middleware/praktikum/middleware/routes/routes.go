package routes

import (
	c "problem2/controllers"
	"problem2/constants"

	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/echo/v4"

)

func New() *echo.Echo {
	e := echo.New()

	e.POST("/login", c.LoginUserController)

	e.POST("/users", c.CreateUserController)
	
	e.GET("/books", c.GetBooksController)
	e.GET("/books/:id", c.GetBookController)
	
	r := e.Group("/jwt")
	r.Use(middleware.JWT([]byte(constants.SECRET_JWT)))
	
	r.GET("/users/:id", c.GetUserDetailControllers)
	r.GET("/users", c.GetUsersController)
	r.DELETE("/users/:id", c.DeleteUserController)
	r.PUT("/users/:id", c.UpdateUserController)

	r.POST("/books", c.CreateBookController)
	r.DELETE("/books/:id", c.DeleteBookController)
	r.PUT("/books/:id", c.UpdateBookController)

	return e
}
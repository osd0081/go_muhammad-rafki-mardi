# (22) Middleware

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Middleware

- Merupakan entitas yang dipasangkan pada proses server
- Dipasangkan diantara ketika klien memberikan response dengan server memberikan
- Berisi fungsi khusus yang berfungsi untuk komunikasi data

|Tipe|Deskripsi|
|---|---|
|Logging Middleware|Mencatat aktivitas server|
|Session Middleware|Apakah klien masih aktif|
|Authentication Middleware|Apakah klien berhak mengakses|
|Custom Middleware|Sebelum komunikasi data client|

```go
middleware.<methodMiddleware()>
```

### Echo Pre

- Middleware akan dieksekusi sebelum request masuk ke controller/echo context:
  - menjadikan http ke https
  - handle trailing slash
  - dls

### Echo Use
- Middleware dieksekusi seleteh mendapat akses echo context:
  - autentifikasi
  - logger
  - dls

### Log Middleware

```go
middleware.logMiddleware()
```

- Berfungsi mencatat yang terjadi dalam request server (inbound dan outbound)
- Footprint, analitics

### Auth Middleware

- Berfungsi untuk identifikasi hak akses user.
- Guna mengamankan data dan informasi 
- Macam:

#### Basic
```
<!-- request header -->
Authentication: basic (username+password)
```
- proses dengan mengirimkan username dan password

#### JWT 
```
<!-- request header -->
Authentication: Bearer (token)
```
- autentikasi dengan memberikan token kepada klien
- token memiliki 3 atribut dan dipisahkan oleh titik

# Praktikum

## Todo list

- [x] Log Implementation
- [x] Implementing JWT Auth for Protecting API

## Logging

- Membuat direktori middleware
- Membuat LogMiddleware

```go
package middlewares

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func LogMiddlewares(e *echo.Echo) {
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `[${time_rfc3339}] ${status} ${method} ${host}${path} ${latency_human}` + "\n",
	}))
}
```
### Implementasikan Log pada semua fungsi CURD

![hasil](./screenshots/LogMIddleware.png)

## JWT

- Membuat JwtMiddleware untuk membuat dan mengambil token akun.
- Membuat constant untuk menyimpan konstanta penting Jwt
- Format untuk memnyusun informasi agar mudah dipahami
- Tambahkan pada controller `Login` dan `DetailUserLogin` untuk membuat dan  memberikan client token ketika melakukan login.
- Menyusun ulang route

### Buat autentikasi (JWT) dari API Section 22, dengan eksepsi:
  - User: POST

![hasil](./screenshots/UserLogin.png)

![hasil](./screenshots/AddUser-NonJwt.png)

![hasil](./screenshots/Jwt-GetUser-Fail.png)

![hasil](./screenshots/Jwt-GetUser-Success.png)

![hasil](./screenshots/Jwt-GetUserById-Fail.png)

![hasil](./screenshots/Jwt-GetUserById-Success.png)

![hasil](./screenshots/Jwt-UpdateUserById-Fail.png)

![hasil](./screenshots/Jwt-UpdateUserById-Success.png)

![hasil](./screenshots/Jwt-DeleteUserById-Fail.png)

![hasil](./screenshots/Jwt-DeleteUserById-Success.png)

  - Book: GET; GET by id

![hasil](./screenshots/Jwt-GetBooksNo-Before.png)

![hasil](./screenshots/Jwt-GetBooksNo-After.png)

![hasil](./screenshots/Jwt-GetBookById-Fail.png)

![hasil](./screenshots/Jwt-GetBookById-Success.png)

![hasil](./screenshots/Jwt-UpdateBookById-Fail.png)

![hasil](./screenshots/Jwt-UpdateBookById-Success.png)

![hasil](./screenshots/Jwt-DeleteBookById-Fail.png)

![hasil](./screenshots/Jwt-DeleteBookById-Success.png)
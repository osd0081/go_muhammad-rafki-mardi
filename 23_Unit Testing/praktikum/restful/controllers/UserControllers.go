package controllers

import (
	"net/http"
	"github.com/labstack/echo/v4"
	"problem2/lib/database"
	"strconv"
	"problem2/models"
	"fmt"
)

func LoginUserController(c echo.Context) error {
	user := models.User{}
	c.Bind(&user)
	userData, e := database.LoginUser(&user)
	if e != nil {
		fmt.Print()
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Internal Server Error",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success login",
		"user": userData,
	})
}

func GetUserDetailControllers(c echo.Context) error {
	id, e := strconv.Atoi(c.Param("id"))
	user, err := database.GetDetailUser(id)
	
	if e != nil || id == 0 || err != nil{
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Internal Server Error",
		})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": user,
	})
}

func GetUsersController(c echo.Context) error {
	users, e := database.GetUsers()
	if e != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Internal Server Error",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": users,
	})
}

func CreateUserController(c echo.Context) error {
	result, e := database.CreateUser(c)
	if e != nil {
		fmt.Print()
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Internal Server Error",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": result,
	})
}

func DeleteUserController(c echo.Context) error {
	id, _:= strconv.Atoi(c.Param("id"))
	result, e := database.DeleteUserById(id)
	if e != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Internal Server Error",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": result,
	})
}

func UpdateUserController(c echo.Context) error {
	// var user models.User
	// c.Bind(&user)

	id, _:= strconv.Atoi(c.Param("id"))
	result, e := database.UpdateUserById(id,c)
	if e != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Internal Server Error",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "success",
		"users": result,
	})
}
package database

import (
	"github.com/labstack/echo/v4"
	"problem2/config"
	"problem2/middlewares"
	"problem2/models"
	"fmt"
)

func GetDetailUser(userId int) (interface{}, error) {
	var user models.User
	if e := config.DB.Find(&user, userId).Error; e != nil {
		return nil, e
	}
	return user, nil
}

func LoginUser(user *models.User) (interface{}, error) {
	var err error
	if err = config.DB.Where("email = ? AND password = ?", user.Email, user.Password).First(user).Error; err != nil {
		return nil, err
	}

	token, err := middlewares.CreateToken(int(user.ID))
	if err != nil {
		return nil, err
	}

	userResponse := models.UserResponse{user.ID, user.Name, user.Email, token}

	if err = config.DB.Save(user).Error; err != nil {
		return nil, err
	}
	return userResponse, nil
}

func GetUsers() (interface{}, error) {
	var users []models.User
	if e := config.DB.Find(&users).Error; e != nil {
		return nil, e
	}
	return users, nil
}

func GetUserById(id int) (interface{}, error) {
	var user models.User
	if e := config.DB.Find(&user, id).Error; e != nil {
		return nil, e
	}
	return user, nil
}

func CreateUser(userData echo.Context) (interface{}, error) {
	var user models.User
	userData.Bind(&user)

	if e := userData.Validate(user); e != nil {
		fmt.Print()
		return nil, e
	}

	if e := config.DB.Save(&user).Error; e != nil {
		return nil, e
	}
	return user, nil
}

func UpdateUserById(id int, userData echo.Context) (interface{}, error) {
	
	var user models.User
	var userUpdate models.User
	userData.Bind(&userUpdate)

	if e := userData.Validate(userUpdate); e != nil {
		fmt.Print()
		return nil, e
	}

	config.DB.First(&user, id)
	var e error
	if result := config.DB.Model(&user).Where("id=?", id).Updates(&userUpdate); result == nil {
		return nil, e
	}
	return user, nil
}

func DeleteUserById(id int) (interface{}, error) {
	var user models.User
	var e error

	if e := config.DB.Find(&user).Error; e != nil {
		return nil, e
	}

	if result := config.DB.Delete(&user, id); result == nil {
		return nil, e
	}
	return "user deleted", nil
}

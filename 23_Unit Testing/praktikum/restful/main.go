package main

import (
	"problem2/config"
	m "problem2/middlewares"
	"github.com/go-playground/validator/v10"
	"net/http"
	"github.com/labstack/echo/v4"
	"problem2/routes"
)

type (
	CustomValidator struct {
		validator *validator.Validate
	}
)

func (cv *CustomValidator) Validate(i interface{}) error {
  if err := cv.validator.Struct(i); err != nil {
    return echo.NewHTTPError(http.StatusBadRequest, err.Error())
  }
  return nil
}

func main() {
	config.InitDB()
	e := routes.New()
	e.Validator = &CustomValidator{validator: validator.New()}
	m.LogMiddlewares(e)
	e.Logger.Fatal(e.Start(":8000"))
}

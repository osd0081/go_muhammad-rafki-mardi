package calculate

import (
	"testing"
)

func TestAddition (t *testing.T){
	if Addition(1,3) != 4 {
		t.Error("Excpected 1 + 3 to equals 4")
	}
}
func TestSubstract (t *testing.T){
	if Substract(1,7) != -6 {
		t.Error("Excpected 1 - 7to equals -6")
	}	
}
func TestMult (t *testing.T){
	if Mult(10,2) != 20 {
		t.Error("Excpected 10 * 2 to equals 20")
	}	
}
func TestDiv (t *testing.T){
	if Div(1,2) != 0.5 {
		t.Error("Excpected 1 / 2 to equals 0,5")
	}
}
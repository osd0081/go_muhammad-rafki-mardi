# (23) Unit Testing

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Unit Testing

- Seorang programmer harus mengetahui bahwa kode yang ditulis bekerja dengan baik
- Seorang programmer harus mengetahui bahwa kode yang dirubah bekerja dengan baik
- Seorang programmer harus memiliki testing yang otomatis yang cakupannya tinggi

### Definisi

- Prosees analisa bagian perangkat lunak untuk membandingkan hasil dengan kondisi kebutuhan serta evaluasi fitur guna mitigasi bug

### Tujuan

- mencegah regresi (perubahan terhadap kode yang sudah berfungsi)
- meingkatkan tingkat kepercayaan refaktori (proses mengubah sistem tanpa mengubah fungsionalitas)
- meningkatkan desain kode
- dokumentasi (dengannya kita mengetahui masukan dan keluaran dari sistem)
- meningkatkan pemahaman tim dalam bekerja terhadap sistem

### Level

|UI|Integration|Unit|
|---|---|---|
|Uji coba UI dari sistem|Uji coba modul yang spesifik terhadap sistem|Uji coba terhadap unit terkecil dalam sistem|

### Framework

- Keranka kerja/library untuk melakukan/membangun sistem testing
- Golang, dapat digunakan `testify` untuk unit testing

### Structure

- Strategi implementasi dalam meletakan kode uji(base) dengan kode penguji

|Pola|Deskripsi|
|---|---|
|Centralize| kode penguji diletakan dalam satu direktori|
|Together| kode penguji terletak pada kode uji(base)|

|*Test File|*\*Test Suites|*\*\*Test Fixture|*\*\*Test Case|
|---|---|---|---|
|File kode penguji (terdapat test suit)|Kumpulkan test case|Script/proses untuk menguji konsistensi dari pengujian|Skenario uji (input dan expected output)|

### Runner

- Aplikasi yang dibuat untuk menjalankan kode penguji(testing)
- Fitur
  - Fitur penjalan file test
  - Watch mode, untuk melakukan test otomatis ketika melakukan perubahan
  - Cepat

### Mocking

- Merupakan tiruan dari fitur yang sedang dikembangkan
- Hal ini memitigasi kerusakan jika fitur dependensi mengalami gangguan
- DONT'S:
  - Uji coba DB (seeder, cleanup)
  - Uji coba Modul/api/sistem eksternal pihak ketiga
  - Objek dengan pengujian mandriri

### Coverage

- Merupakan tolak ukur komponen program mana saja yang sudah dikakukan testing terhadap skenarion" aynga da
- Coverage tool, analisa hasil uji coba unit test
- `Sonar Qube`
- Coach mail, ingkat kesulitan jika melakukan maintenance kode

### Step

- pada go, nama file harus memiliki format (lokasi file, same folder and different/same package):

```go
<namalibrary>_test.go
```
- Format fungsi & parameter
```go
func Test<NameOfOperations> (t *testing.T)
```
- Buat aturan sebagai test cest unit tes.
```go
go test package -cover
```
- Output test
``` go
go test package -coverprofile=cover.out && go tool cover html=cover.out
```

### Skenario dan Test Case

### Test Scenario (What)

- Merupakan gambaran umum terhadap apa yang ingin diuji

### Test Case (How)

- Langkag langkah uji (+) dan (-) dari `test scenario`

# Praktikum

## 1. Membuat unit test pada fungsi pertambahan, pengurangan, perkalian, dan pembagian

```go
package calculate

import (
	"testing"
)

//  Unit test Addition
func TestAddition (t *testing.T){
	if Addition(1,3) != 4 {
		t.Error("Excpected 1 + 3 to equals 4")
	}
}

//  Unit test Substrac
func TestSubstract (t *testing.T){
	if Substract(1,7) != -6 {
		t.Error("Excpected 1 - 7to equals -6")
	}	
}

//  Unit test Mult
func TestMult (t *testing.T){
	if Mult(10,2) != 20 {
		t.Error("Excpected 10 * 2 to equals 20")
	}	
}

//  Unit test Div
func TestDiv (t *testing.T){
	if Div(1,2) != 0.5 {
		t.Error("Excpected 1 / 2 to equals 0,5")
	}
}
```

![hasil](./screenshots/simple/simpleTestProve.png)

## 2. Test Rest (96 persen)

- Membuat testcase tiap test fungsi controller
- Dideklarasikan variabel global untuk input body json resquest

![hasil](./screenshots/rest/restProve.png)
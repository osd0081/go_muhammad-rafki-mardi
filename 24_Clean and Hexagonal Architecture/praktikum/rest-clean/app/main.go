package main

import (
  "github.com/labstack/echo/v4"
	"rest-clean/config"
	"github.com/jinzhu/gorm"

	_userHttpDelivery "rest-clean/user/delivery/http"
	_userRepo "rest-clean/user/repository/mysql"
	_userUcase "rest-clean/user/usecase"
)

var (
	db *gorm.DB = config.SetupDatabaseConnection()
	userRepository = _userRepo.NewMysqlUserRepository(db)
	userUsecase = _userUcase.NewUserUseCase(userRepository)
	userHandler = _userHttpDelivery.NewUserHandler(userUsecase)
)

func main () {
	e := echo.New()
	userHandler.Route(e)
	e.Logger.Fatal(e.Start(":8000"))
}
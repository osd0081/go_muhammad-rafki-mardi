package http

import (
	"net/http"
	"github.com/labstack/echo/v4"
	"rest-clean/domain"
	"fmt"
)

type ResponseError struct {
	Message string `json:"message"`
}

type UserHandler struct {
	UUsecase domain.UserUsecase
}

func NewUserHandler(uc domain.UserUsecase) *UserHandler {
	return &UserHandler{
		UUsecase: uc,
	}
}

func (u *UserHandler) Route (e *echo.Echo){
	e.GET("/users",u.GetAll)
	e.POST("/users",u.Create)
}

func (u *UserHandler) GetAll(c echo.Context) error {
	listUs, err := u.UUsecase.GetAll()
	if err != nil {
		return c.JSON(GetStatusCode(err), ResponseError{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, listUs)
}

func (u *UserHandler) Create(c echo.Context) (err error) {
	var newUs domain.User
	err = c.Bind(&newUs)
	
	user, err := u.UUsecase.Create(newUs)
	if err != nil {
		fmt.Println(err)
		return c.JSON(GetStatusCode(err), ResponseError{Message: err.Error()})
	}
	return c.JSON(http.StatusCreated, user)
}

func GetStatusCode(err error) int {	
	switch err {
	case domain.ErrInternalServerError:
		return http.StatusInternalServerError
	case domain.ErrNotFound:
		return http.StatusNotFound
	case domain.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
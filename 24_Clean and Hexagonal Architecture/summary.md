# (24) Clean and Hexagonal Architecture

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Clean Architecture (Arsitektur perangakat lunak)

### Definition

- Seperation of concern using layer, to build a modular, scalable. maintanable and testtable application.

### Why

- Memuat ruang untuk peningkatan di masa depan
- Knowledge transfer is important during development
- Easy to do unit test

### Hexagonal

- Pembatasan client dengan layanan berupa presenter _application program interface_
- Pembatasan storage service dengan layanan berupa _storage program interface_
- Layanan atau bussiness logic terdapat di antara API dan SPI

### Constraint

|Indepentent of Frameworks|Testable|Of UI|Of Database|Of Any External|
|---|---|---|---|---|
|Framework tidak memiliki batasan|Bussiness logic dapat diuji|UI bisa berubah tanpa mengubah sistem|Tidak bergantung logika binis, kita dapat mengubah basisdata dengan mudah|Sistem tidak mengetahui external sistem(cache client side, dls)|

### Keuntungan

- Memiliki acuan standar
- Pengembangan cepat
- Mocking database untuk unit testing menjadi mudah
- Mudah untuk menukar prototipe dengan hasil akhir

### Milestones

- Monolith --> Mircoservice

### Layer Clean Architecture

|Layer|Description|
|---|---|
|Entitas|Objek bisnis yang diatur|
|Use Case/Domain|Logika bisnis|
|Controller/Presentation|Mengatur interaksi pengguna|
|Driver/Data|Mengatur sistem penyimpanan (data aplikasi, data in memory, data cache)|

### Approach

- Single Module
- Modul per-layer
- Modul perfitur
- Modul perfitur dipecah kembali

## Domain Driven Design (Teknik desain perangkat lunak)

- Dilatar belakangi dengan interaksi antara `SE` dengan `Expert Bidang`
- Hal diatas diampu dengan `Ubiquitous Language`:
  - Dimulai dengan memproses solusi (berdasarkan `Ubiquitous Language`) secara perlahan
  - Bounded Context, dalam suatu bisnis hal tersebut harus berkomunikasi dengan entitas tunggal

# Praktikum

- Tiap layanan memiliki direktori sendiri
- Dalam direktori tersebut terdapat layer clean architecture masing masing
- domain mendefinisikan kasus bisnis

![hasil](./screenshots/clean-est.png)
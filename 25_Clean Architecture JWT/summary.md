# (25) Clean Architecture JWT

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

- middleware ditambahkan pada delivery dari service
- logic validasi dilakukan di layer usecase
- untuk melakukan validasi dari data pada database, layer repository digunakan untuk mengambil data pada database

# Praktikum

Membuat sebuah jwtmiddleware service yang berisi berikut

```go

package middleware

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	jwt "github.com/golang-jwt/jwt"
	"time"
)

type jwtMiddleware struct {
	key string
}

type JWTService interface {
	GenerateToken() (string, error)
	JwtMiddleware() echo.MiddlewareFunc
}

// loose coupling
func NewJwtService (secretKey string) JWTService {
	return &jwtMiddleware{
		key: secretKey,
	}
}

// mengaktifkan jwt pada route tertentu
func (h *jwtMiddleware) JwtMiddleware () echo.MiddlewareFunc {
  return middleware.JWTWithConfig(middleware.JWTConfig{
    SigningMethod: "HS256",
		SigningKey: []byte(h.key),
	})
}

// memberikan jwt pada saat login berhasil
func (h *jwtMiddleware) GenerateToken () (string, error) {
	claims := jwt.MapClaims{}
	claims["exp"]= time.Now().Add(time.Hour*1).Unix()

	key := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return key.SignedString([]byte(h.key))
}
```

Membuat method login pada Usecase

```go
func (u *userUsecase) Login(user domain.User) (bool, error) {
	if ok, err := u.Validate(&user); !ok {
		return false, err
	}
	
	userAuth, err := u.userRepo.GetByQuery(user)
	if err != nil {
		return false, err
	}

	var e error
	if userAuth.Password != user.Password {
		return false, e
	}
	return true, nil
}
```

Membuat method login pada Handler
```go
func (u *UserHandler) Login (c echo.Context)  error {
	var user domain.User
	err := c.Bind(&user)
	var val bool
	val, err = u.UUsecase.Login(user)
	if err != nil {
		return c.JSON(GetStatusCode(err), ResponseError{Message: err.Error()})
	}
	if val == false {
		return c.JSON(http.StatusUnauthorized, ResponseError{Message: "Unauthorized"})
	}
	token, e := u.UJwt.GenerateToken()
	if e != nil {
		return c.JSON(GetStatusCode(err), ResponseError{Message: err.Error()})
	}
	return c.JSON(GetStatusCode(err),ResponseError{Message: token})
}
```

Membuat method Query pada repo
```go
func (m *mysqlUserRepository) GetByQuery(user domain.User) (res domain.User, err error) {	
	var userAuth domain.User
	if err = m.connection.Where("email = ?", user.Email).First(&userAuth).Error; err != nil {
		return res, err
	}
	return userAuth, nil
}
```

![hasil](./screenshots/login_Fail.png)
![hasil](./screenshots/login_Success.PNG)
![hasil](./screenshots/getAll_Fail.PNG)
![hasil](./screenshots/getAll_Success.PNG)


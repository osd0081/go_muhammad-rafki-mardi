# (26) Clean Architecture Unit Test

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

- Menggunakan mocker untuk menghasilkan return dari method yang diluar unit testing
- Menggunakan sqlMocekr untuk menghasilkan in memory database guna pengujian unit testing
- untuk membuat mocker dijalankan kode berikut

```
mockery --all --keeptree
```

# Praktikum

## 1. Hasil Usecase 

[Cover profile](./praktikum/rest-clean/user/usecase/cover)

![hasil](./screenshots/user_usecase.png)

## 2. Hasil Repository 

[Cover profile](./praktikum/rest-clean/user/repository/mysql/cover)

![hasil](./screenshots/user_repository.png)

## 3. Hasil Http 

[Cover profile](./praktikum/rest-clean/user/delivery/http/cover)

![hasil](./screenshots/user_http.png)

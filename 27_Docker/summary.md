# (27) Docker

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Container

- Container berorientasi aplikasi
- VM orientasinya adalah mesin itu sendiri
- Dengan adanya container, aplikasi berdiri diatas platform sesuai kebutuhan aplikasi tersebut sehingga menghemat sumber daya.

## Infrastruktur

### Client

- CLI interaksi

### Docker Host

- Docker daemon, mengatur:
  - Container
  - Images

### Registery

- Semacam repository untuk mengpush image

## Kapabilitas

- Dokcer file sebagai template dan konfigurasi dari image 
- Membuat image (bundle aplikasi, dependensi dan kebutuhannya)
- Docker hub (Image repository)
- Docker host (semacam gitpull)

## Sintaks

![hasil](./screenshots/summary_1.png)

# Praktikum

## Install Docker & Docker Compose 

![](./screenshots/1_installDocker%26Compose.png)

## Buat Docker File

![](./screenshots/2_dockerFile.png)

## Clone/Buat docker image dari program rest

![](./screenshots/3_clone%26integrate.png)

## Buat Container dari image

![](./screenshots/4_buildContainer.png)

## Buat Docker Repo

[Docker hub](https://hub.docker.com/repository/docker/incarico/alta_go_27)

![](./screenshots/5_docker%20repo.png)

## Deploy di mesin lokal

![](./screenshots/6_deployLocal.png)

## Deploy dengan docker compose app & mysql

### Success `up`

![](./screenshots/7_deployComposeApp%26MySQL-2.png)

### Success create

![](./screenshots/7_deployComposeApp%26MySQL-3.png)

### Success login

![](./screenshots/7_deployComposeApp%26MySQL-4.png)

### Success Get Users

![](./screenshots/7_deployComposeApp%26MySQL-5.png)
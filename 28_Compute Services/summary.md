# (28) Compute Service

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Deployment

Merupakan kegiatan untuk menyebar layanan yang telah dikembangkan.

## Strategi

||Big-bang/Replace|Rollout|Blue/Green|Canary|
|---|---|---|---|---|
|+|Implementasi mudah, perubahan instan|Aman dan downtime sedikit|perubahan cepat, tidak ada isu versi|Aman, cepat rollback|
|-|Beresiko downtime lama|Ketika dideploy 2 versi berjalan bersama laten error, deploy dan rollback lebih lama, tidak ada request control sehinga request sama dengan versi sebelumya|butuh banyak resource, dan intense testing|deploy 100% butuh waktu lama|

## Tahap deploy

Local -> Test Env -> UAT Env -> Production Enc

- Tahap 2 & 4 dibantu oleh 3rd party software (misal travis ci)

## Tambahan

- Devops sebagai karir yang berfokus pada CI/CD
- Shell script untuk otomasi proses dari deploy/managemen

# Praktikum

Buat terlebih dahulu IAM yang memiliki privilage RDS dan EC2 kemudian login dengan user IAM

![hasil](./screenshots/IAM.png)

---

## EC 2

1. Membuat EC2 dan Security group untuk EC2

- Buat EC2 dengan pilihan free-tier

![hasil](./screenshots/EC%202.png)

- Buat ssh key ketika mengenerate instance

![hasil](./screenshots/SSH%20KEY.jpeg)

- Perhatikan untuk EC2 harus membuat security group **Outbound**

![hasil](./screenshots/SG-EC2.png)

2. SSH ke remote VM

![hasil](./screenshots/SSH%20Terminal.png)

- perintah diatas mengartikan bahwa kita akan mengakses VM dengan kredensial berupa `.pem` file
- jika kita mengakses dengan password maka kita tinggal menghilangkan `-i`

3. Deploy Program

- deploy dilakukan dengan gitwork flow

![hasil](./screenshots/WF.png)
![hasil](./screenshots/WF-Secrets.png)

---

## RDS

1. Buat DB di RDS

![hasil](./screenshots/RDS.png)

- Pastikan free tier terpilih

![hasil](./screenshots/RDS%20domain%20example.jpeg)

2. Migrasi DB

- Dengan domain RDS yang sudah terbuat daftarkan security group ke cluster rds berupa **inbound**

![hasil](./screenshots/SG-RDS.png)

- jalankan printah berikut

```
    psql --host auth-api.dasdasdasd.ap-southeast-1.rds.amazonaws.com --username postgres
```

- Lalu buat database postgres yang ingin kita hubungkan dengan aplikasi kita
- Update secret yang ada di github ()

3. Hubungkan RDS dengan EC2
- SSH ke VM, lalu update file `config.env` kemudian build ulang aplikasi

```
go build
```

- lalu jalankan program

```
systemctl start main
```

# (29) CI-CD

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Continuous Integration

- Proses otomasi untuk integrasi kode terhadap perubahan yang berlaku
- Kegiatan (life cycle) ini dibantu oleh source control.

## Continuous Delivery Deployment

- Merupakan tingkatan lanjut dari CI karna proses delivery-nya.
- Setiap perubahan yang bersifat terverifikasi maka kode langsung dideploy

## [Tools](https://landscape.cncf.io/)

![cycle](./screenshots/Cycle.png)

# Praktikum

## AWS

- Lakukan registrasi AWS

## AWS - IAM

- Masuk ke IAM, buat user baru untuk menghindari pembuatan instance oleh root user

![AWS](./screenshots/IAM.png)
![AWS](./screenshots/IAM-adduser.png)

- Buat UserGroup baru & beri permission `EC2FULL`

![AWS](./screenshots/IAM-group.png)
![AWS](./screenshots/IAM-ec2full.png)

- Masukan User ke UserGroup baru
- Ketika IAM berhasil terbuat, unduh file CSV untuk melakukan autentikasi nantinya.

![AWS](./screenshots/IAM-success.png)

- Lakukan login via link yang terdapat dalam file CSV

## AWS - EC2

- Ganti region ke region yang lebih dekat

![AWS](./screenshots/region.png)

- Buat EC 2 seperti summary

![AWS](./screenshots/EC2-Summary.png)

- Edit security group sesuai dengan yang diminta [github tugas](https://github.com/goFrendiAsgard/alta-batch-3-ec2)

![AWS](./screenshots/EC2-Security.png)

- Kemudian instance dan `Pair-key` terbuat. Simpan pairkey ke folder yang aman
- Simpan pula host dan username dari instance guna SSH

![AWS](./screenshots/EC2-secret.png)

## GITHUB

- Fork [repo tugas](https://github.com/goFrendiAsgard/alta-batch-3-ec2)
- Aktifkan actions pada repo tersebut

![Git](./screenshots/GITHUB-action.png)

- Dengan data EC2 yang sudah kita simpan kita buat variabel dengan isinya berikut

|Data|Deskripsi|
|---|---|
|KEY|**private key** yang digenerate dari pembuatan insta|
|PORT|port ssh (buat 22 sesuai konfigurasi security group EC2)|
|USERNAME|username dari ssh (didapat saat `connect` ke AWS instance|
|HOST|string setelah `@` ketika SSH|

![Git](./screenshots/GITHUB-secrets.png)

## Provisioning & Deployment

- lakukan SSH ke instance
- Jalankan perintah berikut

```
<!-- Update & Instal Docker & Compose -->
sudo apt-get -y update
sudo apt-get -y install docker.io
sudo chmod 777 /var/run/docker.sock
sudo apt-get -y install python3-pip
sudo pip3 install -y docker-compose 

<!-- Clone Repo -->
git clone https://github.com/ixtza/alta-batch-3-ec2.git ~/app

wget https://golang.org/dl/go1.17.2.linux-amd64.tar.gz
sudo rm -rf /usr/local/go 
sudo tar -C /usr/local -xzf go1.17.2.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
go version

echo 'PATH=$PATH:/usr/local/go/bin' >> ~/.bashrc
```

- Jangan lupa karna terdapat dependensi pada repo, lakukan perintah berikut

```
cd /app
go test -v ./...
go mod tidy
```

- Agar tidak terjadi kekurangan dependensi ketika melakukan CI/CD maka lakukan deployment pertama secara manual

```
cd app
docker-compose down && docker-compose up --build -d
```

## Action Testing

- Buat perubahan terhadap `readme.md` atau file lainnya
- Push perubahan, dan lihat dashboard action. Jika berhasil akan terlihat seperti berikut.

![Action](./screenshots/ACTION-Compleate.png)

![Action](./screenshots/GITHUB-proof.png)

## Tambahan

- Sebelum push (perubahan baru pada readme di instance)

![Tambahan](./screenshots/ACTION-sebelum.png)

- Sesudah

![Tambahan](./screenshots/ACTION-setelah.png)

[Screenshoot lainnya](./screenshots/)
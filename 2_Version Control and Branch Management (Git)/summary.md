# (2) Version Control and Branch Management (Git)

## Daftar isi
- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary
- Versioning untuk mengatur versi dari kode suatu program yang dibantu oleh VCS, SCM, RCS. VCS sendiri sudah mengalami perjalanan sejarah sejak 1972
- Tools _VCS_ yang sering digunakan adalah `git` (dibuat oleh Linus Trovalds). Dibantu `remote repository` sebagai penunjang sistem _versioning_ terdistribusi salah satunya `github`.

|Sintaks|Deskripsi|
|---|---|
|git config|melakukan konfigurasi umum dari git|
|git init|inisiasi git pada suatu direktori|
|git remote|melakukan konfigurasi terhadap repo _remote_|
|git clone|melakukan duplikasi dari suatu repo|
|git pull|untuk menarik perubahan yang ada di repository remote/branch lokal|
|git push|untuk mengirim perubahan local repo ke remote repo|
|git merge|melakukan penyatuan branch|
|git add|memasukan perubahan ke staging area|
|git commit|memsukan perubahan ke repository|
|git log|melihat histori commit|
|git reset|**hard** ,mengembalikan kondisi kode sebelum commit dan menghilangkan perubahaan. **soft** ,seperti hard namun tidak menghilangkan perubahan yang terjadi|
- Dalam melakukan commit terkadang harus ada file yang diabaikan demi menjaga ruang penyimpanan atau besar file yang akan ditaruh di _remote_ repo, karnanya digunakan fila `.gitignore`
- Alur dalam berkolaborasi kode harus dilakukan diluar kode utama misal cabang `perkembangan`, didalamnya lagi dibuat cabang yang masing masing memiliki tugas untuk mengembangkan fitur-fitur berbeda.
- Dengen _workflow_ diatas, lazimnya melakukan commit dengan opsi `--no-ff`
- Ketika ingin menyatukan branch tersebut, dilakukan fitur _pull request_ pada github.

# Praktikum

## 1. Membuat github repository ✅

Pembuatan repositori github kurang lebih mengikuti panduan [berikut](https://docs.github.com/en/get-started/quickstart/create-a-repo), hal yang berbeda adalah **membiarkan repository kosong (tanpa inisialisasi `readme.md`)**

Kemudian _clone_ `remote repo` atau hubungkan `local repo` dengan `remote repo`, pada bahasan ini yang dilakukan adalah menghubungkan `local repo`. Cara yang dilakukan adalah memasukan perintah berikut di terminal yang berjalan di `local repo`.

```
git remote add origin <remote repository URL>
```

lakukan verifikasi dengan printah
```
git remote -v
```

Sekarang, `local repo` sudah terhubung dengan `remote repo`.

## 2. Membuat brancing **main**, development, featureA, dan featureB ✅

_[Why not `master` ?](https://github.com/github/renaming)_

Pada branch master, dibuat file `index.html` kemudian masukan code berikut.
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```
### **Git Push**
kemudian `save` lalu lakukan `add` `commit` dan `push`

```
git add .
git commit -m "initial commit"
git push origin main
```

---
### **Git Pull**
Untuk mengkonfirmasi keberhasilan push lakukan,
```
git pull origin main
```
Akan muncul pesan bahwa `HEAD` pada `local repo` sudah sama dengen `HEAD` pada `remote repo`

---
### **Git stash**
Misal kita menambahkan data berikut dalam tag `body` pada html

```html
</head>
<body>
    <h1>Ini main</h1> <!-- perubahan baru -->
</body>
</html>
```
Namun, kita ragu bahwa perubahan tersebut layak untuk di-`commit` di-`main` pada saat ini. Maka dilakukan `stash` guna menyimpan perubahan tersebut sementara

```
git stash push 
```
Perubahan tersebut akan hilang seolah tidak pernah terjadi, namun sebenarnya tersimpan. Untuk melihat seluruh simpanan `stash` jalankan perintah berikut.

```
git stash list
```

kemudian jika ingin mengembalikan perubahan yang tersimpan dalam `stash`, lakukan

```
git stash apply --index <indeks stash pada stash list>
```

---

Kemudian dibuat branch `development` **terhadap branch `main`**. lalu pindah ke branch `development`.

```
git branch development
git switch development
```
Maka akan didapat file-file yang ada dan isinya sama dengan branch `main`

---
Pada branch `development`, edit file `index.html` menjadi seperti berikut.
```html
...
</head>
<body>
    <h1>featureA</h1>
    <p>

    </p>
    <h1>featureB</h1>
    <p>

    </p>
</body>
</html>
```
lakukan `commit` pada branch `development`.

---
Kemudian saatnya kita membuat branch `featureA` dan `featureB`. **Pembuatan branch ini harus dilakukan ketika kita berada pada branch `development`.**
```c
// pada branch `development`
git branch featureA
git branch featuerB
```
## 3. Penanganan konflik ketika `featureB` di merge setelah `featureA` merge ke `development`✅

Kita pergi ke branch `featureA` dan edit `index.html` menjadi seperti berikut

```html
<!--index.html pada branch `featureA`-->
...
</head>
<body>
    <h1>featureA</h1>
    <p>
        some featureA
    </p>
...
```
### **Git merge**
Lakukan `commit` pada branch `featureA`. Kemudian, ke branch `development` lalu lakukan `merge`.
```
git merge featureA --no-ff
```
Argumen `--no-ff` pada `merge` berarti `pointer` tracker pada branch `development` tidak berpindah ke `pointer` branch `featureA` namun mereka membuat sebuah `snapshot` baru.

---
Jika sudah, pergi ke branch `featureB` lalu edit `index.html` menjadi seperti berikut.

```html
    <h1>featureA</h1>
    <p>
        oops mistakenly edited this line
    </p>
    <h1>featureB</h1>
    <p>
        some featureB
    </p>
```
Perhatikan dalam sekenario diatas, kita mengisi tag paragraph dari _featureA_, hal ini sengaja guna memancing conflict nantinya.

Simpan perubahan tersebut lalu lakukan `commit`.

Kemudian pergi ke branch `development` lalu jalankan printah berikut
```
git merge featureB --no-ff
```
Maka akan muncul conflict, untuk melakukan penyelesaian, buka `VSCODE` (karna penulis menggunakan IDE ini) lalu pergi ke line code dimana terjadi conflict. Akan terlihat tampilan berikut:

![d2-bukti2-praktikum](https://user-images.githubusercontent.com/74223938/154068335-729918f7-6391-4d6f-b140-bfefc00070af.png)

Pilih opsi yang ada di atas sesuai kebutuhan, untuk saat ini kita pilih `accept current changes`

# 4. Implementasi push, pull, stash, dan merge ✅
Terlaksanakan sembari mengerjakan [nomer 2](#2-membuat-brancing-main-development-featurea-dan-featureb-%E2%9C%85) dan [nomer3](#3-penanganan-konflik-ketika-featureb-di-merge-setelah-featurea-merge-ke-development).

![stashproof](./screenshots/stash.png)

# 5. Merge no fast forward ✅

Pergi ke branch `main`, lalu jalankan
```
git merge development --no-ff
```
Maka ketika kita buka `log` dengan perintah
```
git log --oneline --graph --all
```
terbukti bahwa `snapshot` yang dihasilkan adalah baru alias non liner merge.

![d2-bukti3-praktikum](./screenshots/log.png)

package cgomath

import (
	"github.com/stretchr/testify/assert"
	"testing"
	// "fmt"
)

func TestPassed(t *testing.T) {
	t.Run("pass score", func(t *testing.T) {
		assert.Equal(t, Passed(90), true, "The result must be true")
		assert.Equal(t, Passed(100), true, "The result must be true")
		assert.Equal(t, Passed(60), true, "The result must be true")
	})
	t.Run("fail score", func(t *testing.T) {
		assert.Equal(t, Passed(75), false, "The result must be false")
	})
}

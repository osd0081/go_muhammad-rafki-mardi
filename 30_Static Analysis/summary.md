# (30) Static Analysis

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Definisi

- Analisis yang dilakukan tanpa eksekusi perangakat lunak
- Sebagai code review

### Data Flow Analysis

- Teknik untuk mengumpulkan informasi data ketika statis/simulasi
  - Common Subexpression
  - Left(sisa) variable, variabel didefinisi namun tidak digunakan

### Taint Analysis
- Fitur untuk meningkatkan keamanan, mencegah pengguna melakukan RCE
- Variable harus dienkapsulasi

## Tools
- Linter
- Sonarqub
- Dls

### Linter
- Memberikan marking untuk memberikan klarifikasi terhadap penulisan kode yang rawan

### SonarQube
- Opensource
- Program pihak ketiga yang melakukan code integration

# Praktikum

## Manual Analysis

```go
package main

func Passed(score int) bool {
	switch {
	case score > 80:
		return true
	case score > 70:
		return false
	default:
		return true
	}
}
```

Hasil Analisis:

- Mengubah package main menjadi package dari repo/project

```go
package cgomath

...
```

- Terdapat _unreachable code_ pada baris 6 hingga 8. sehingga bisa dibuat seperti berikut.

```go
func Passed(score int) bool {
	switch {
	case score > 70:	
    return score > 80
	default:
		return true
	}
}
```

- Atau menggunakan if secara keseluruhan:

```go
func Passed(score int) bool {
  if score > 70{
    return score > 80
  } else{
		return true
  }
}
```

## Sonar Cube Analysis

Step:

- Jalankan sonarcube pada docker
- Ubah kredensial admin pertama kali
- Buat project dengan pilih `Manually`
- Isi nama project sesuai keinginan (biarkan nama token)
- Ketika project terbuat, pilih `Locally` sebagai media analisis kode
- Beri nama dan simpan token guna kredensial ke container sonarqube.

![hasil](./screenshots/CSproject.png)

![hasil](./screenshots/CSproject_token.png)

- Pilih `other` sebagai bahasa pemrograman yang akan diananlisis dan pilih OS mesin lokal.
- Simpan sintaks `sonar-scanner`
- Kembali ke project, lalu buat file `sonar-project.properties`

```properties
sonar.projectKey=cgomath
sonar.projectName=cgomath

sonar.sources=.
sonar.exclusions=**/*_test.go

sonar.test=.
sonar.test.inclusions=**/*_test.go

sonar.go.coverage.reportPaths=coverage
```

|Hal|Deksripsi|
|---|---|
|sonar.projectKey|key yang digunakan untuk mengubungkan ke project di sonarqube|
|sonar.pronectName|Nama project|
|sonar.source|Sumber project yang akan dilakukan static analysis|
|sonar.exclusion|File yang akan diabaikan untuk dianalisis|
|sonar.test|File yang akan diuji|
|sonar.test.inclusions|Sama dengan yang diatas namun menggunakan regex|
|sonar.go.coverage.reportPaths|untuk menyertakan coverage pada laporan sonarqube|

- Buat unit test dari project, lalu jalankan

```
go test -v -coverpkg=./... -coverprofile=coverage.out ./...
```

- Kemudian jalankan perintah berikut, tujuannya adalah untuk menjalankan `sonar-scanner` pada container

```
docker run --rm -e SONAR_HOST_URL="http://localhost:9000" -e SONAR_LOGIN="kode-an" -v "lokasi/project:/usr/src" --network host -m 1g sonarsource/sonar-scanner-cli:latest
```

Hasil:

![hasil](./screenshots/proof-success.png)
![hasil](./screenshots/cli-success.png)
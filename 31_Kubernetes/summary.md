# (31) Kubernetes

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Kubernetes

- Merupakan aplikasi open-source untuk otomasi deploy, pengaturan, dan mengatur aplikasi yang terkontainer

### Kapabilitas

- Network dan loadblancing
- Horisontal scaling
- Rollout dan rollbacks
- Secret dan manjemen konfigurasi

### Bukan Kapabilitas Kubernetes

- Mendeploy kode dan membangun aplikasi
- Menyediakan middleware
- Menyediakan logging, monitoring atau himbauan
- Aplikasi yang didukung tidak dibatasi

![res](./screenshots/res_1.png)

### Namespace

- Merupakan sumber daya kumpulan layanan pods atau bisa disebut juga kumpulan nodes

### Pods

- Kumpulan (atau satu) kontainer yang berada di satu koneksi

### Labels

- Mekanisme untuk mengatur objek kubernetes

### Deployment

- Supervisor untuk pods, memberikan kontrol yang lebih terdefiniisi

### Rolling updates

- Update pada deploy menjadi minim downtime dengang memperbarui pods secaara berkala
- Pod baru akan dijadwalkan pada node yang resourcenya tersedia

### Secrets

- Sama dengan github secrets untuk menyimpan variable penting

### Service

- Abstraksi dari kumpulan pods agar dapat saling berkomunikasi


### Ingress

- API object yang mengatur akses eksternal ke servis misal http




# Praktikum

## Clone/Buat docker image dari program rest

![](./screenshots/3_clone%26integrate.png)

## Buat Container dari image

![](./screenshots/4_buildContainer.png)

## Buat Docker Repo

[Docker hub](https://hub.docker.com/repository/docker/incarico/alta_go_27)

![](./screenshots/5_docker%20repo.png)
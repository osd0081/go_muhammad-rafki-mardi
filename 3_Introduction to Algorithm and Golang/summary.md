# (3) Introduction to Algorithm and Golang

# Daftar isi

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Apa itu Algo
- Menyusun instruksi untuk memecahkan masalah
- Prosedur komputasi  baik dengan input tertentu untuk menghasilkan output sesuai tujuan
- Implementasi
	- Cek prima
	- Sorting
	- Searching
## Karakteristik Algo
- Batasan constrain
- Instruksi terdefinisi dengan baik (concise)
- Efektif dan efisian
## Dasar 
- Sequenctial
	- Proses harus berurut
- Brancing 
	- Untuk pengkondiisian
- Loop 
	- Melakukan proses secara berulang
## Pseudocode
- Deskripsi sederhana algoritma
- Tools pembandtu pseudocode adalah flowchart
## Flowchart
Sebagai tools dalam memetakan algoritma menjadi diagram interaktif

# Praktikum

## No.1 & No. 2

https://whimsical.com/ntroduction-to-algorithm-P3b397GrxH2z2aUm8jLAQK

## No.3 & No. 4

```go
package main
import (
	"fmt"
)
func main()  {
	fmt.Println("Hello world!")
}
```
![Bukti Screenshot](https://github.com/ixtza/go_muhammad-rafki-mardi/blob/main/3_Introduction%20to%20Algorithm%20and%20Golang/screenshots/3-4.png)

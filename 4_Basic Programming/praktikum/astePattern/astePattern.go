package main

import "fmt"

func playWithAsterisk(n int){
	m:=n
	c:=n-1
	for i := 1; i <= n ; i++{
		for j := 0; j < m; j++{
			if j < c{
				fmt.Print(" ")
			}else if i % 2 == 0{
				if j % 2 == 0{
					fmt.Print(" ")
				}else{
					fmt.Print("*")
				}
			}else if i % 2 != 0{
				if j % 2 == 0{
					fmt.Print("*")
				}else{
					fmt.Print(" ")
				}
			}
		}
		fmt.Println()
		m++;
		c--;
	}
}

func main (){
	playWithAsterisk(5)
}
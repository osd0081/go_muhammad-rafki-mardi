package main

import (
	"fmt"
)

// fungsi rekursif
func pangkat (base, ordo int) int {
	if ordo == 0{
		return 1
	}
	return base*pangkat(base,ordo-1)
}

func main () {
	// var base, ordo int

	fmt.Println(pangkat(2,3))
	fmt.Println(pangkat(5,3))
	fmt.Println(pangkat(10,2))
	fmt.Println(pangkat(2,5))
	fmt.Println(pangkat(7,3))

	// fmt.Print("Masukan basis: ")
	// fmt.Scanf("%d\n",&base)
	// fmt.Print("Masukan ordo: ")
	// fmt.Scanf("%d",&ordo)

	// fmt.Printf("%d^%d bernilai %d", base,ordo,pangkat(base,ordo))
}
package main

import "fmt"

func main()  {
	var bilangan int
	// scanf untuk menerima input data
	fmt.Println("Masukan nilai bilangan asli:")
	fmt.Scanf("%d\n", &bilangan)


	// suatu bilangan N memiliki faktor terbesar berupa N/2
	// sehingga dilakukan loop hinggan N/2
	for i:=1;i<=bilangan/2;i++{
		if(bilangan%i==0){
			fmt.Println(i)
		}
	}
	fmt.Println(bilangan)
}
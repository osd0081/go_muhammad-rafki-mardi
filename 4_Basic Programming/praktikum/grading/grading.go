package main

import(
	"fmt"
)

func main(){
	// input
	var studentScore int = 80
	if 80<=studentScore && studentScore<=100{
		fmt.Println("A Grade")
	}	else if 65<=studentScore && studentScore<80{
		fmt.Println("B Grade")
	}	else if 50<=studentScore && studentScore<65{
		fmt.Println("C Grade")
	}	else if 35<=studentScore && studentScore<50{
		fmt.Println("D Grade")
	}	else if 0<=studentScore && studentScore<34{
		fmt.Println("E Grade")
	}	else{
		fmt.Println("Invalid score")
	}
}
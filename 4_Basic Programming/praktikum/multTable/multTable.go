package main

import (
	"fmt"
)

func cetakTabelPerkalian(number int) {
	for i := 1; i <= number; i++ {
		for j := 1; j <= number; j++ {
			fmt.Print(j*i,"\t")
		}
		fmt.Println()
	}
}

func main (){
	var n int
	fmt.Print("Masukan besar tabel: ")
	fmt.Scanf("%d\n",&n)
	cetakTabelPerkalian(n)
}
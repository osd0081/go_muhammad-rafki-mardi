package main

import (
	"fmt"
	// "bufio";
	// "os"
)

func palindrome(input string) bool{
	
    if len(input)==0 || len(input)==1{
		return true
	}
	
	isPalindrome := true

	for i, _ := range input{
		if input[i] != input[len(input)-1-i]{
			isPalindrome = false
			break
		}
	}
	return isPalindrome
}

func main(){
    
    // var isPalindrome bool
	// var input string

	fmt.Println(palindrome("civic"))
	fmt.Println(palindrome("katak"))
	fmt.Println(palindrome("kasur rusak"))
	fmt.Println(palindrome("mistar"))
	fmt.Println(palindrome("lion"))
	fmt.Println(palindrome("jublie a eilbuj"))
    
    // fmt.Print("Masukan string: ")
    // fmt.Scanf("%s\n",&input)
	// in := bufio.NewReader(os.Stdin)
	// (line,err) := in.ReadString('\n')
    // isPalindrome = palindrome(input)

    // switch isPalindrome {
    // case true:
    //     fmt.Println("Palindrome")
    // case false:
    //     fmt.Println("Non palindrome")
    // }
}
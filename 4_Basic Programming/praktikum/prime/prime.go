package main

import "fmt"

func primeNumber(number int) bool{
    var isPrime bool
    // menggunakan prinsip seperti mencari bilangan faktor
    for i := 2; i <= number/2+1; i++ {
        // memanfaatkan branching untuk mengecek setiap iterasi
        if(number%i==0 && number != i){
            isPrime = false
			break
        }else{
            isPrime = true
        }
    }
    return isPrime
}

func main(){
    
    fmt.Println(primeNumber(11))
    fmt.Println(primeNumber(13))
    fmt.Println(primeNumber(17))
    fmt.Println(primeNumber(20))
    fmt.Println(primeNumber(35))
    // var number int
    
    // fmt.Print("Masukan bilangan asli: ")
    // fmt.Scanf("%d",&number)
    // isPrime :=primeNumber(number)

    // switch isPrime {
    // case true:
    //     fmt.Println("Bilangan Prima")
    // case false:
    //     fmt.Println("Bukan bilangan Prima")
    // }
}
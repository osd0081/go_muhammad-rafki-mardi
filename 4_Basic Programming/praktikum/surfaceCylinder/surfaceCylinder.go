package main

import (
	"fmt"
)

func main() {
	// input	
	T:=20.0
	r:=4.0

	const(
		pi float64 = 3.14
	)

	surface := 2*pi*r*(r+T)

	fmt.Printf("Surface area of a cylinder with,\n\tHeight = %.2f\n\tRadius = %.2f\nis %.2f\n",T,r,surface)
}

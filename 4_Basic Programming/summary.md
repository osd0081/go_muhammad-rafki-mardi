# (4) Basic Programming

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## Intro

- Mulai 2007 (open 2009)
- Golang sebagai lower level program untuk menyediakan service kepada layanan lain (system programming & application programs)
- Why?
	- Simpel
	- Static (c.) dynamic (js)
	- Lightweight syntax
	- concuren

## Variable Golang

- Variable golang hanya bisa menyimpan tipe data yang sama
- Terdapat 3 tipe data primitive
	- Numeric
		- Memiliki tipe numerik yang beragam (untuk menyesuaikan penggunaan memori)
	- Boolean
	- String
- Deklarasi
	- Menggunakan := 
		- Tidak boleh deklarasi variable global
	- Var `nama` `tipe data` =
		- `nama` bisa berupa mulit variable(nama)
		- `tipe data` berupa primitive
	- Zero value
		- Bol false
		- Float 0.0
		- String ""
		- Int 0
	- Const
		- Menggunakan key 'const'
			- Const `nama` `tipe` =
		- Multi const declaration
			- Const (
					`nama` `tipe` = …
					`nama1` 
					)
					- variabel ini akan memiliki tipe dan konten yang sama dengan variabel `nama` diatas
	- Array
		- Dengan [`jumlah data`]`tipedata`(`data-data`)
			- Jumlah data
				- … = jumlah data sama dengan jumlah `data-data`
				- (kosong) bukan array tapi slice
				- N (N bilangan asli)
			- `tipe data`
				- Primitive
				- Map
				- Array
				- slice

## Operand and Operator
- Operand merupakan nilai
- Operator adalah operasi
	- (+)
		- Numerik (penjumlahan)
		- String (concatenante)
	- Expression merupakan definisi  baik itu fungsi maupun variabel.

## Branching
- Terdiri dari dua sintaks
	- If
	- Switch
- If
	- Case dari if tidak diwrap oleh tanda kurung
- Switch
	- Case hanya di cek sekali
	- Satu line case dapat menampung multi value (dengan tipe data yang sama)
	- Tidak dibutuhkan break di setiap akhir case
	- Jika tidak ada default/case yang memenuhi dsa maka switch dilewati.

## Looping

- Golang hanya memiliki loop for
- Parameter loop tidak dibungkus oleh tanda kurung
- Walaupun hanya for kita dapat kresikan loop ini seolah-olah mirip dengan loop lain (while,dsbg)
- Untuk meloop sebuah array/string dengan for bisa menggunakan range
	- For `index`,`nilai` := range `nama array`
		- `index` merupakan index elemen array/string
		- `nilai` merupakan elemen pada index ke `index`

# Praktikum

## 1. Menghitung Luas
[Source code](praktikum/surfaceCylinder/surfaceCylinder.go)
Solusi yang diimplementasikan adalah menggunakan metode adhoc.
![bukti](./screenshots/surfaceOfCylinder.png)
## 2. Grade Nilai
[Source code](./praktikum/grading/grading.go)

Memanfaatkan braching `if-else` *statement*

![bukti](./screenshots/grading.png)
## 3. Faktor Bilangan
[Source code](./praktikum/factors/factors.go)

- Menggunakan fungsi `scanf` dari package `fmt` 
- Menggunakan ide solusi bahwa suatu bilangan N memiliki faktor terbesar yakni sekitar `N/2` sehingga dilakukan loop hingga `N/2`
- Dengan `if` *statement* jika hasil modulo tiap iterasi menghasilkan 0 maka dicetak indeks iterasi tersebut.

![bukti](./screenshots/factors.png)
## 4. Bilangan Prima
[Source code](./praktikum/prime/prime.go)

- Dibuat fungsi `primeNubmer` yang menerima `int` dan mengembalikan nilai `boolean` 
- Menggunakan fungsi `scanf` dari package `fmt` 
- Didalam fungsi `primeNumber` dengan prinsip perulangan seperti mencari bilangan faktor, dilakukan iterasi hingga `N/2+1`
- Dengan `if-else` *statement* dilakukan pengecekan dengan modulo, jika menghasilkan 0 maka diset nilai `boolean` menjadi `false` iterasi berhenti dengan `break` dan nilai `boolean` dikembalikan.
- Jika iterasi hingga `N/2+1`, nilai `boolean` berupa `true`

![bukti4](./screenshots/prime.png)
## 5. Palindrome
[Source code](./praktikum/palindrome/palindrome.go)

- Dibuat fungsi `palindrome` yang menerima `string` sebagai argumen dan mengembalikan `boolean`
- Kasus khusus ketika panjang string berupa 0 atau 1 maka langsung dikembalikan `true`
- Jika bukan kasus khusus masuk kedalam iterasi, iterasi menggunakan `range` terhadap input.
- jika terdapat karakter yang berbeda, iterasi diberhentikan dan dikembalikan nilai `false`

![bukti5](./screenshots/palindrome.png)
## 6. Exponential
[Source code](./praktikum/exponen/exponen.go)

- Menggunakan cara rekursif dengan *decrementing* ordo
- Jika ordo bernilai 0, kembalikan nilai 1

![bukti6](./screenshots/exponen.png)
## 7. Play With Asterisk
[Source code](./praktikum/astePattern/astePattern.go)

- Dibuat fungsi void yang menerima nilai `N` dan bertugas untuk mencetak pola
- iterasi menggunakan *inner loop* `for`
- dibuat nilai flag berupa `c` dan `m`, `c` untuk kapan dilakukan pencetakan asterisk. `m` untuk menentukan banyaknya elemen yang terdapat pada sebuah baris.
	- untuk suatu input `N`, maka terdapat maksimal sebanyak `2N - 1` elemen (baik itu *whitespace* maupun asterisk)
	- nilai `m` increment tiap iterasi pertama sedangkan `c` sebaliknya

![bukti7](./screenshots/astePattern.png)
## 8. Cetak Tabel Perkalian
[Source code](./praktikum/multTable/multTable.go)

- Dibuat fungsi void `cetakTabelPerkalian`
- menggunakan konsep `inner loop`
- dalam loop kedua, setiap iterasi, cetak hasil perkalian inteks loop pertama dengan inteks loop kedua.
- indeks seluruh loop dimulai dari 1

![bukti8](./screenshots/mulTable.png)

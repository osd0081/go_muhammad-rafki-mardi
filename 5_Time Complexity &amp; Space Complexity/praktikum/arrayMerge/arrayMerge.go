package main

// credit 
// - https://www.geeksforgeeks.org/binary-search-a-string/
// - https://ispycode.com/GO/Strings/Compare-strings-lexicographically
// complexity o(N log (N))

import (
	"fmt"
)

func binaryArraySearch(leftIndex, rightIndex int,arrayA []string, target string) bool {	

	if leftIndex <= rightIndex{
		mid:= leftIndex + (rightIndex - leftIndex) / 2
		if arrayA[mid] == target || arrayA[leftIndex] == target || arrayA[rightIndex] == target{
			return true
		}
		if arrayA[mid] < target {
			binaryArraySearch(mid + 1, rightIndex ,arrayA , target)
		}else{
			binaryArraySearch(leftIndex,mid - 1 ,arrayA , target)
		}
	}
	return false
}

func arrayMerge(arrayA,arrayB []string) []string  {
	var mergedArray []string
	mergedArray = arrayA
	for i := 0; i < len(arrayB); i++ {
		if binaryArraySearch(0,len(arrayA)-1,arrayA,arrayB[i])==false{
			mergedArray = append(mergedArray,arrayB[i])
		}
	}
	return mergedArray
}

func main()  {
	fmt.Println(arrayMerge([]string{"king","devil jin","akuma"},[]string{"eddie","steve","geese"}))
	fmt.Println(arrayMerge([]string{"sergei","jin"},[]string{"jin","steven","brian"}))
	fmt.Println(arrayMerge([]string{"alissa","yoshimatsu"},[]string{"devil jin","yoshimatsu","alissa","law"}))
	fmt.Println(arrayMerge([]string{},[]string{"devil jin","sergei"}))
	fmt.Println(arrayMerge([]string{"hwoarang"},[]string{}))
	fmt.Println(arrayMerge([]string{},[]string{}))
}
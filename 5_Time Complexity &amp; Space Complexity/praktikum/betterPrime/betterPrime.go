package main

// credit GFG
// complexity naive o(sqrt(N))

import (
	"fmt";
	"math"
)

// it is defined if N is a prime then surelly there wont be any factors
// even until sqrt(N)

func primeNumber(n int) bool{
	if n<=1{
		return false;
	}

	param := math.Sqrt(float64(n))

	for i:=2;i<=int(param);i++{
		if n%i == 0{
			return false
		}
	}
	return true
}

func main()  {
	// var bilangan,tc int
	// fmt.Println("Masukan jumlah testcase:")
	// fmt.Scanf("%d\n",&tc)
	// for tc!=0{
	// 	fmt.Println("Masukan nilai yang akan dicek:")
	// 	fmt.Scanf("%d\n", &bilangan)
	
	// 	switch isPrime(bilangan) {
	// 	case true:
	// 		fmt.Println("Bilangan Prima")
	// 	case false:
	// 		fmt.Println("Bukan bilangan Prima")
	// 	}
	// }
	fmt.Println(primeNumber(1000000007))
	fmt.Println(primeNumber(13))
	fmt.Println(primeNumber(17))
	fmt.Println(primeNumber(20))
	fmt.Println(primeNumber(35))
}
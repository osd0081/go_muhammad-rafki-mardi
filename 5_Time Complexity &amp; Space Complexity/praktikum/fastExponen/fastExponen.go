package main

import (
	"fmt";
	"strconv"
)

// credit https://www.johndcook.com/blog/2008/12/10/fast-exponentiation/
// complexity o(2log(N))

func pow(x int,n int64) int {
	var bin string
	const mod int = 1000000007
	temp := x
	bin = strconv.FormatInt(n,2)

	// for n!=1 {
	// 	ev := n%2
	// 	if ev == 1{
	// 		bin = "1"+bin
	// 	}else{
	// 		bin = "0"+bin
	// 	}
	// 	n/=2
	// }

	for i:=1 ; i< len(bin) ; i++{
		if string(bin[i]) == "1"{
			x = ((x% mod)*(x% mod)) % mod
			x = ((x% mod)*(temp%mod)) % mod
		}else{
			x = ((x% mod)*(x% mod)) % mod
		}
	}
	return x
}

func main()  {
	// var x,n,tc int
	// fmt.Println("Masukan jumlah testcase:")
	// fmt.Scanf("%d\n",&tc)
	// for tc != 0{
	// 	fmt.Println("Masukan basis dan ordo (secara berturut turut pisahkan dengan spasi):")
	// 	fmt.Scanf("%d %d\n",&x,&n)
	// 	fmt.Printf("Hasil (dalam mod 10^9+7):\n%d\n",primeNumber(x,n))
	// 	tc--
	// }
	fmt.Println("Hasil (dalam mod 10^9+7)")
	fmt.Println(pow(2,3))
	fmt.Println(pow(5,3))
	fmt.Println(pow(10,2))
	fmt.Println(pow(2,5))
	fmt.Println(pow(7,3))
}
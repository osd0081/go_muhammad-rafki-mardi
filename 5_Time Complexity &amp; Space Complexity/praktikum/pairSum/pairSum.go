package main

import (
	"fmt"
)

func PairSum(array []int, target int) []int {
	var indexes = []int{}
	array = array[:len(array)]
	i:=0
	j:=len(array)-1
	for {
		if array[i]+array[j]<target{
			i++
		}
		if array[i]+array[j]>target{
			j--
		}else if array[i]+array[j]==target{
			indexes = append(indexes,i)
			indexes = append(indexes,j)
			break
		}
		if i>j {break}
	}
	return indexes
}

func main()  {
	// var input,target,tc int
	// fmt.Println("Masukan jumlah testcase:")
	// fmt.Scanf("%d\n",&tc)
	// for tc!=0{
	// 	array := []int{}
	// 	fmt.Println("Masukan masukan elemen array (pisahkan dengan spasi):")
	// 	for{
	// 		n,err := fmt.Scanf("%d",&input)
	// 		array = append(array,input)
	// 		if n==0||err!=nil{
	// 			break
	// 		}
	// 	}
	// 	fmt.Println("Masukan target:")
	// 	fmt.Scanf("%d\n",&target);
	// 	fmt.Println(PairSum(array,target))
	// 	tc--
	// }
	fmt.Println(PairSum([]int{1,2,3,4,6},6))
}
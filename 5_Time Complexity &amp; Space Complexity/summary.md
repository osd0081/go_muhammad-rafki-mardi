# (5) Time Complexity &amp; Space Complexity

# Daftar Isi

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. Time Complexity
- Seberapa lama sebuah operasi dominan yang di lakukan
- Operasi dominan merupakan operasi primitif

|Cara menentukan operasi dominan:|
|------------|
|gunakan nontasi `big-O`|
|cari operasi yang berulang|

|Tipe `big-O`| Keterangan |
|---|---|
|O(1)|Eksekusi berjalan sebesar 1 kali|
|O(N)|Untuk sebuah input `N`, operasi primitif algoritma berjalan sebanyak `N` kali|
|O(N+M)|Ketika sebuah operasi primitif algoritma selalu berjalan sebanyak `N + M`|
|O(log N)|Ketika operasi primitif, untuk suatu input `N` akan dipecah dengan dibagi 2|
|O(N^2)|Operasi primitif dijalankan dengan `inner loop` dengan batas yang sama|

- Kompleksitas lainnya, 
    - `O(N!)` *factorial time* 
    - `O(2^N)` *exponential time*

Kompleksitas diatas cocok ketika nilai `N` kecil

![Kurva Big-O](./screenshots/time-diagram.png)

- Komputer mampu melakukan eksekusi sebesar 10^8 eksekusi tiap detik
- Dalam kontes online *time limit* biasanya sebesar 1 hingga 10 detik
- Dalam beberapa referensi, untuk kompleksitas tertentu baiknya dibangun algoritma dengan kompleksitas seperti dibawah:

|Input|Expected Complexity|Avg Time|
|---|---|---|
|N<=1000000|`O(n)` atau `O(N Log N)`|>1detik|
|N<=10000|`O(N^2)`|~1detik|
|N<=500|`O(N^3)`|~1detik|

## B. Space Complexity

- Ketika melakukan operasi, sebuah data hasil input atau kalkulasi dimasukan kedalam sebuah *space* yang disebut `memori`
- Lazimnya membuat program dengan jumlah variable yang konstan

# Praktikum

## 1. Bilangan Prima

[Source code](./praktikum/betterPrime/betterPrime.go)

- Menggunakan pendekatan bahwa suatu bilangan prima tidak memiliki nilai akar quadrat, sehingga dilakukan iterasi hingga indeks ke `sqrt(N)` dimana N adalah suatu bilangan asli.
- Membuat special case ketika input kurang dari 2.

![Bukti](./screenshots/betterPrime.png)

## 2. Fast Exponentiation

[Source code](./praktikum/fastExponen/fastExponen.go)

- Menggunakan referensi [berikut](https://www.johndcook.com/blog/2008/12/10/fast-exponentiation/)
- Didefinisikan bahwa untuk suatu nilai exponen (nilai pangkat), kita bisa memanfaatkan nilai binernya.
- Dimulai dari indeks kedua sebelah kiri suatu nilai biner,
    - ketika elemen indeks tersebut 0, kuadratkan dengan nilainya sendiri
    - ketika elemen indeks 1, kuadratkan kemudian kalikan dengan bilangan basis

![bukti](./screenshots/fastExponen.png)

## 3. Array Merge

[Source code](./praktikum/arrayMerge/arrayMerge.go)

- Menggunakan binary array search untuk mencari indeks elemen yang duplikat
- Iterasi array kedua terhadap array pertama
- Cek duplikasi string menggunakan prinsip `lexicographic`
- Ketika nilai `lexicographic` besar dari indeks yang diperiksa, binary upperbound
- Ketika lebih kecil, binary lowerbound
- Jika false (bukan duplikat) dilakukan append ke array baru, sebaliknya tidak

![bukti](./screenshots/arrayMerge.png)

## 4. Angka Muncul Sekali

[Source code](./praktikum/uniqueNum/uniqueNum.go)

- Buat sebuah array `flag` sebesar 10, dan slice `unique` 
- Iterasi tiap byte dari string
- Ketika pertama kali dijumpa masukan nilai indeks kedalam 'flag' dan append ke array `unique`
- Ketika nilai elemen dalam `flag` besar dari 0, ambil elemen `flag`(nilai indeks pada unique) dan ubah isi index `unique` menjadi -1 (tanda ada duplikat)
- Untuk membersihkan elemen, dibuar iterasi baru, ketika elemen `unique` bukan -1 append ke array baru.

![bukit](./screenshots/uniqueNum.png)

## 5. Pair with Target Sum

[Source code](./praktikum/pairSum/pairSum.go)

- Suatu fungsi menerima `array` dan target berupa `int` 
- Disiapkan variable untuk menyimpan *upperbound* dan *lowerbound* `array`
- Dilakukan iterasi `for` terhadap *bound* yang sudah didefinisikan
    - Ketika nilai penjumlahan elemen *lower* dan *upper* *bound* besar dari target, *lowerbound decriment*
    - Sebaliknya *upperbound increament*
- Nilia *bound* dikembalikan sebagai hasil pencarian

![bukti](./screenshots/pairSum.png)
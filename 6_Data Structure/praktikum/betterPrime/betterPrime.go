package main

// credit GFG
// complexity naive o(sqrt(N))

import (
	"fmt";
	"math"
)

// it is defined if N is a prime then surelly there wont be any factors
// even until sqrt(N)

func primeNumber(n int) bool{
	if n<=1{
		return true;
	}

	param := math.Sqrt(float64(n))

	for i:=2;i<=int(param);i++{
		if n%i == 0{
			return false
		}
	}
	return true
}

func main()  {
	
	fmt.Println(primeNumber(1000000007))
	fmt.Println(primeNumber(1500450271))
	fmt.Println(primeNumber(1000000000))
	fmt.Println(primeNumber(10000000019))
	fmt.Println(primeNumber(10000000033))
	
	var bilangan int
	fmt.Scanf("%d\n", &bilangan)

	switch primeNumber(bilangan) {
	case true:
		fmt.Println("Bilangan Prima")
	case false:
		fmt.Println("Bukan bilangan Prima")
	}
	

}
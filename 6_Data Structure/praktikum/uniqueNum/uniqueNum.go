package main

import (
	"fmt"
)

// buat sebuah array `flag` sebesar 10 
// iterasi tiap byte dari string
// ketika pertama kali dijumpa masukan nilai indeks kedalam 'flag' 
// ketika nilai elemen dalam 'flag' besar dari 0, ubah ke -1 (tanda ada duplikat)

// kompleksitas 2N

func angkaMunculSekali(numbers string) []int {
	var flag = [10]int{}
	var unique = []int{}
	var ans = []int{}
	for i:=0;i<len(numbers);i++{
		if flag[int(numbers[i])-48] == 0{
			flag[int(numbers[i])-48] = i + 1
			unique = append(unique,int(numbers[i])-48)
		} else if flag[int(numbers[i])-48] > 0 {
			unique[flag[int(numbers[i])-48]-1] = -1
			flag[int(numbers[i])-48] = -1
			unique = append(unique,-1)
		}
	}
	for i:=0;i<len(unique);i++{
		if unique[i]!=-1{
			ans = append(ans,unique[i])
		}
	}
	return ans
}

func main()  {
	// var tc int
	// var numbers string
	// fmt.Println("Masukan jumlah testcase:")
	// fmt.Scanf("%d\n",&tc)
	// for tc!=0{
	// 	fmt.Println("Masukan nilai yang akan dicek:")
	// 	fmt.Scanf("%s\n",&numbers)
	// 	fmt.Println(munculSekali(numbers))
	// 	tc--
	// }	
	fmt.Println(angkaMunculSekali("1234123"))
	fmt.Println(angkaMunculSekali("76523752"))
	fmt.Println(angkaMunculSekali("12345"))
	fmt.Println(angkaMunculSekali("1122334455"))
	fmt.Println(angkaMunculSekali("0872504"))
}
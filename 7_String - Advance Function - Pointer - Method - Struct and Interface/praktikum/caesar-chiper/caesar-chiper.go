package main

import (
	"fmt"
)

func caesar(offset int, input string) string {
	offset = offset % 26
	var result string
	for _, el := range input {
		result = fmt.Sprint(result,string(((el%rune('a')+rune(offset))%26)+rune('a')))
	}
	return result
}

func main(){
	fmt.Println(caesar(3,"abc"))
	fmt.Println(caesar(2,"alta"))
	fmt.Println(caesar(10,"alterraacademy"))
	fmt.Println(caesar(1,"abcdefghijklmnopqrstuvwxyz"))
	fmt.Println(caesar(1000,"abcdefghijklmnopqrstuvwxyz"))
}

package main

import (
	"fmt"
)

func Compare(a,b string)string{

	max := len(b)
	check := b
	target := a
	var result string
	if len(a) > max{
		max = len(a)
		check = a
		target = b
	}

	for i := 0; i < max; i++ {
		if check[i:len(target)] == target {
			result = target
			break
		}
		if i+len(target) > max {
			break
		}
	}
	return result
}

func main()  {
	fmt.Println(Compare("AKA","AKASHI"))
	fmt.Println(Compare("KANGOORO","KANG"))
	fmt.Println(Compare("KI","KIJANG"))
	fmt.Println(Compare("KUPU-KUPU","KUPU"))
	fmt.Println(Compare("ILALANG","ILA"))
}

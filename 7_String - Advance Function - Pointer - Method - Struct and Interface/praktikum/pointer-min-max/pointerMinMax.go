package main

import (
	"fmt"
)

func getMinMax(numbers ... *int) (min int, max int) {
	min = int(^uint(0)>>1)
	max = -min

	for _, number := range numbers {
		if(min>*number){
			min = *number
		}
		if(max<*number){
			max = *number
		}
	}
	
	return max, min
}

func main()  {
	var a1,a2,a3,a4,a5,a6,min,max int
	fmt.Scan(&a1)
	fmt.Scan(&a2)
	fmt.Scan(&a3)
	fmt.Scan(&a4)
	fmt.Scan(&a5)
	fmt.Scan(&a6)
	max, min = getMinMax(&a1,
		&a2,
		&a3,
		&a4,
		&a5,
		&a6,
	)

	fmt.Println("Nilai max:", max)
	fmt.Println("Nilai min:", min)
}
package main

import(
	"fmt"
	"errors"
	"strings"
)

func validateName(input string)(bool,error){
	if strings.TrimSpace(input) == ""{
		return false, errors.New("Name cannot be empty")
	}
	return true, nil
}

func validateScore(input int)(bool,error){
	if input < 0 || input >100 {
		return false, errors.New("Invalid score")
	}
	return true, nil
}

func catch(){
	if r := recover(); r!=nil{
		fmt.Println("Error occured",r)
	}else{
		fmt.Println("Application running perfectly")
	}
}

type Student struct {
	name []string
	score []int	
}

func (s Student) Average() float64 {
	var avg float64
	for _, sco := range s.score{
		avg += float64(sco)/float64(len(s.score))
	}
	return avg
}

func (s Student) Min() (min int, name string){
	min = int(^uint(0)>>1)
	for in, sco := range s.score{
		if(min>sco){
			min = sco
			name = s.name[in]
		}
	}
	return min, name
}

func (s Student) Max() (max int, name string){
	max = -int(^uint(0)>>1)
	for in, sco := range s.score{
		if(max<sco){
			max = sco
			name = s.name[in]
		}
	}
	return max, name
}

func main()  {
	var a = Student{}
	defer catch()

	for i:=0;i<6;i++{

		var name string
		fmt.Printf("Input %d Student's Name : ",i+1)
		fmt.Scan(&name)

		if valid, err := validateName(name); valid{
			a.name = append(a.name,name)
		}else{
			panic(err.Error())
		}

		var score int
		fmt.Print("Input "+name+" Score : ")
		fmt.Scan(&score)
		
		if valid, err := validateScore(score); valid{
			a.score=append(a.score,score)
		}else{
			panic(err.Error())
		}
	}

	fmt.Println("\nAverage Score Student is", a.Average())
	scoreMax, nameMax := a.Max()
	fmt.Println("Max Score Student is : "+nameMax+" (",scoreMax,")")
	scoreMin, nameMin := a.Min()
	fmt.Println("Min Score Studnet is : "+nameMin+" (",scoreMin,")")
}
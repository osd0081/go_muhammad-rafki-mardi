package main

import (
	"fmt"
)

type student struct{
	name string
	nameEncode string
	score int
}

type Chiper interface{
	Encode() string
	Decode() string
}

func (s *student) Encode() string {
	var nameEncode = ""
	for _, el := range s.name {
		nameEncode = fmt.Sprint(nameEncode,string((26-el%rune('a'))+rune('a')-1))
	}
	return nameEncode
}

func (s *student) Decode() string {
	var nameDecode = ""
	for _, el := range s.nameEncode {
		nameDecode = fmt.Sprint(nameDecode,string((26-el%rune('a'))+rune('a')-1))
	}
	return nameDecode
}


func main()  {
	var menu int
	var a = student{}
	var c Chiper = &a
	fmt.Print("[1] Encrypt \n[2] Decrypt \nChoose your menu : ")
	fmt.Scan(&menu)
	switch menu {
	case 1:
		fmt.Print("\nInput Student's Name : ")
		fmt.Scan(&a.name)
		fmt.Print("\nEncode of Student's Name "+a.name+" is : "+c.Encode())
	case 2:
		fmt.Print("\nInput Student's Encode Name : ")
		fmt.Scan(&a.nameEncode)
		fmt.Print("\nDecode of Student's Name "+a.nameEncode+" is "+c.Decode())
	default:
		fmt.Println("Wrong input name menu")
	}
}
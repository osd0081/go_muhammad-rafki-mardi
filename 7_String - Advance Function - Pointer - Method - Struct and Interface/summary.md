# (7) String - Advance Function - Pointer - Method - Struct and Interface

# Daftar Isi

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## 1. String

- Golang menyediakan fungsi khusus untuk mengolah data string melalui package `strings` yang dapat kita import

|Sintaks|Fungsi|
|---|---|
|`strings.len()`|`int` : mengembalikan panjang string|
|`==\|<=\|>=`|`int` : membandingkan string|
|`strings.contains()`|`boolean` : memindai apakah suatu string merupakan substring dari suatu string|
|`strings[<starting_index>:<lenght>]`|`string` : mengembalikan string sepanjang `starting_index` hingga `length`|
|`strings.Replace(<strings>,<from>,<into>,<how_much_el>)`|`string` : mengganti el pada string|
|`+`| `string` : untuk insert/append string|

## 2. Advance Function

- `Variadic Function`, ketika terdapat function yang menerima parameter namun tidak diketahui jumlah parameter tersebut.
    - seperti operasi matematika dimana bilangan bulat yang dimasukan berjumlah bervariatif
- Golang tidak dapat melakukan overloading function/method, karnanya `Variadic Function` membantu dalam permasalahan ini.
```go
func (data... int) int {
    // code
}
```

- `Anonymous Function` merupakan fungsi yang tidak memiliki nama. Fungsi ini berisi suatu proses tertentu yang hanya dipakai sekali atau pada blok tertentu
    - fungsi ini dapat disimpan kedalam variabel

```go
func main () {
    // anonymous function
    func(/*param*/){
    
    }(/*operand yang akan dioper ke param*/)
}
```

- `Closure Function` merupakan salah satu bentuk `Anonymous Function` yang memiliki kemampuan isolasi data dan reference

```go
func NewCounter() func{} int {
    counter := 0
    return func () int {
        counter++
        return counter
    }
}
```

- `Defer Function` adalah sebuah fungsi yang bila mana diawali dengan sintaks `defer` maka fungsi tersebut akan dijalankan di akhir proses.
    - Untuk multi `defer` function, maka bersifat `LIFO` sedangkan untuk proses didalam fungsi tersebut akan berjalan seperti kode pada umumnya
    - Umumnya digunakan ketika mengakses operasi pada database

```go
func main (){
    defer func (){
        fmt.Println("kedua")
    }()
    defer func (){
        fmt.Println("pertama")
    }()
    fmt.Println("utama")
    // output : 
    // utama 
    // pertama 
    // kedua
}
```

## 3. Pointer

- Merupakan variable yang menyimpan alamat dari sebuah data.
- Untuk suatu tipe data primitive, alamatnya dapat diakses dengan key `&`
- Untuk suatu tipe data pointer, untuk mengakses data harus menggunakan key `*`
- Tipe data pointer merupakan tipe data berdasarkan `reference`

```go
var numberA int = 4
var numberB *int = &numberA

fmt.Println("numberA (value)   :", numberA)  // 4
fmt.Println("numberA (address) :", &numberA) // 0xc20800a220

fmt.Println("numberB (value)   :", *numberB) // 4
fmt.Println("numberB (address) :", numberB)  // 0xc20800a220

numberA = 7

fmt.Println("numberA (value)   :", numberA)  // 7
fmt.Println("numberA (address) :", &numberA) // 0xc20800a220

fmt.Println("numberB (value)   :", *numberB) // 7
fmt.Println("numberB (address) :", numberB)  // 0xc20800a220
```

## 4. Struct

- Karna golang tidak mengenal `OOP` maka `struct` membantu untuk menutupi kekurangan ini
- Akses data pada struct menganut prinsip sama dengan `C`

```go
type structName struct{
    var someData int
}

func main(){
    // cara 1
    var someStruct structName
    someStruct.someData = 0
    fmt.Println(someStruct.someData)
    // cara 2
    var anotherStruct structName{
        someData: 2,
    }
    // cara 3
    // cara ini harus melakukan full construction
    moreStruct := structName{12}
}
```

## 5. Method

- Fungsi yang ditempel pada sebuah `type/struct`
- Hal ini membantu kita dalam membangun golang dengan `OOP` style.
- Menghindari name conflict
- Meningkatkan *Readibility*

```go
type Employee struct{
    firstName, lastName string
}

func (e Employee) FullName () string{
    return e.firstName + " " + e.lastName
}

func main(){
    var someStruct Employee{
        firstName: "Ikfat",
        lastName: "Muzaqi",
    }
    fmt.Println(anotherStruct.Fullname)
}
```

# Praktikum

[Hasil Submisi Pertama](https://www.interviewzen.com/interview/13ababbc-e585-4847-9f30-f609b7b4d5a0)

[Hasil Submisi Kedua](https://www.interviewzen.com/interview/3487e8f2-d12e-4c9b-b58b-9e107df8b055)

## 1. Compare String

[Source code](./praktikum/compare-string/compare-string.go)
- Menentukan mana string terpendek agar proses dapat berjalan konsisten
- Terhadap string yang lebih panjang dilakukan iterasi sebanyak panjang string terpanjang.
- Dibuat variable string `check` guna menampung pergeseran string saat iterasi
- Dengan percabangan `if`, ketika string `check` sama dengan `target` maka iterasi berhenti dan mengembalikan nilai `check`
- `Ketika` indexing diluar batas maka kembalikan nilai kosong

![hasil](./screenshots/compare-string.png)

## 2. Caesar Cipher

[Source code](./praktikum/caesar-chiper/caesar-chiper.go)
- Dengan bantuan `range` elemen dari string diiterasi
- Tiap elemen dicast menjadi `rune` dan dimodulo oleh `rune("a")` yang kemunian ditambah offset yang sudah dimodulo 26. Hasil akhir tersebut akan ditambahkan ke `rune("a")`.

![hasil](./screenshots/caesar-cipher.png)

## 3. Pointer Swap

[Source code](./praktikum/pointer-swap/pointer-swap.go)
- Menggunakan prinsip pass by reference, digunakan key `*` untuk menukan alamat dari alamat data yang dipass kedalam argumen fungsi

![hasil](./screenshots/pointer-swap.png)

## 4. Min Max using Pointer

[Source code](./praktikum/pointer-min-max/pointerMinMax.go)
- Menggunakan prinsip yang mirip dengan [problem 3](#3-pointer-swap).
- Untuk bisa menentukan nilai `min` dan `max`, nilai tersebut diinisiasi dengan `|int(^uint(0)>>1)|`
- Untuk mengakses nilai guna melakukan percabangan `if` digunakan key `*` pada data pointer

![hasil](./screenshots/pointer-min-max.png)

## 5. Student Score

[Source code](./praktikum/student-score/studentScore.go)
- Menggunakan `struct` dan `method` agar data yang terproses sebatas `Student`
- Untuk menentukan nilai `Min` dan `Max` sama dengan prinsip [nomer 4](#4-min-max-using-pointer), fungsi ini tidak hanya mengembalikan nilai terkait namun juga nama siswa juga
- Untuk menghitung nilai `Average` dicast data inputan nilai menjadi `float64` agat hasil akurat dan presis
- Menggunakan fungsi `validation` untuk memunculkan `error`.
- error tersebut kemudian dimasukan kedalam `panic`yang kemudian dengan `defer func` akan dilakukan `recovery` guna mencetak error yang terjadi.

![hasil](./screenshots/student-score.png)

## 6. Subtitution Cipher

[Source code](./praktikum/sub-cipher/subCipher.go)
- Memanfaatkan interface yang menyediakan method kosong.
- Dengan memanfaatkan interface, sebuah objek yang memiliki operasi sama dengan objek lain dapat memiliki nama yang sama.
- Dalam hal ini variable interface dibuat yang mana menyimpan alamat dari data yang akan diproses.
- Untuk `Encode` dan `Decode`, tiap iterasi elemennya dengan `range` dilakukan operasi dengan formula berikut 
```go
string((26-el%rune('a'))+rune('a')-1)
```

![hasil](./screenshots/subtitution-cipher.png)
package main

import(
	"fmt"
	"sort"
)

// credit https://www.geeksforgeeks.org/quick-sort/

// func Partition(arr *[]int, low,high int) int {
	
// 	pivot := (*arr)[high]
// 	i := low - 1 
// 	for j := low; j < high; j++ {
// 		if (*arr)[j] <= pivot{
// 			i++
// 			(*arr)[i] , (*arr)[j] = (*arr)[j] , (*arr)[i]
// 		}
// 	}

// 	(*arr)[i + 1] , (*arr)[high] = (*arr)[high] , (*arr)[i + 1]

// 	return i + 1
// }

// func QuickSort(arr *[]int, low,high int)  {
// 	if (low < high){
// 		pi := Partition(arr, low, high)

// 		QuickSort(arr,low,pi-1)
// 		QuickSort(arr,pi,high)
// 	}
// }

func MaximumBuyProduct(money int, productPrice []int)  {
	// QuickSort(&productPrice,0,len(productPrice)-1)

	sort.Slice(productPrice,func (i,j int) bool {
		return productPrice[i] < productPrice[j]
	})

	var product,sum int

	for _,el := range productPrice{
		sum += el
		if(sum > money){
			break
		}else{
			product++
		}
	}

	fmt.Println(product)
}

func main()  {
	MaximumBuyProduct(50000,[]int{25000, 25000, 10000, 14000})
	MaximumBuyProduct(30000,[]int{15000,10000,12000,5000,3000})
	MaximumBuyProduct(10000,[]int{2000,3000,1000,2000,10000})
	MaximumBuyProduct(4000,[]int{7500,3000,2500,2000})
	MaximumBuyProduct(0,[]int{10000,30000})
}

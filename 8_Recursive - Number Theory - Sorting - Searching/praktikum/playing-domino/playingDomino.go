package main

import(
	"fmt"
	"sort"
)

// func Partition(arr*[][]int,low,high int) int {

// 	pivot := (*arr)[high][0] + (*arr)[high][1]
// 	i := low - 1

// 	for j := low; j < high; j++ {
// 		if (*arr)[j][0] + (*arr)[j][1] <= pivot{
// 			i++
// 			(*arr)[i],(*arr)[j] = (*arr)[j],(*arr)[i]
// 		}
// 	} 

// 	(*arr)[i + 1],(*arr)[high] = (*arr)[high],(*arr)[i + 1]

// 	return i + 1
// }

// func QuickSort(arr*[][]int,low,high int)  {
// 	if (low < high){
// 		pi := Partition(arr,low,high)

// 		QuickSort(arr,low,pi-1)
// 		QuickSort(arr,pi,high)
// 	}
// }

func playingDomino(cards [][]int,deck[]int) interface{} {
	
	// QuickSort(&cards,0,len(cards)-1)

	sort.Slice(cards,func (i,j int) bool {
		return cards[i][0]+cards[i][1] > cards[j][0]+cards[j][1]
	})

	for i := 0; i < len(cards); i++ {
		if (deck[0]==cards[i][0]||deck[0]==cards[i][1]||deck[1]==cards[i][0]||deck[1]==cards[i][1]){
			return cards[i]
		}
	}

	return "tutup kartu"
}

func main()  {
	fmt.Println(playingDomino([][]int{[]int{6,5},[]int{3,4},[]int{2,1},[]int{3,3}},[]int{4,3}))
	fmt.Println(playingDomino([][]int{[]int{6,5},[]int{3,3},[]int{3,4},[]int{2,1}},[]int{3,6}))
	fmt.Println(playingDomino([][]int{[]int{6,6},[]int{2,4},[]int{3,6}},[]int{5,1}))
}
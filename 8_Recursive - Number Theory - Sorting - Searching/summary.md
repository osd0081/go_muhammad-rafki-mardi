# (8) Recursive - Number Theory - Sorting - Searching

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A. Recursive

- Merupakan metode pemanggilan fungsi sendiri secara berulang
- Metode rekursif dapat memperjelas penyelesaian masalah yang sekiranya mampu dipecahkan dengan demikian
- Berikut hal yang patut dipertimbangkan ketika membangun algoritma rekursif

|Pertimbangan|Deskripsi|
|---|---|
|Base case|apa kasus termudah dari masalah (untuk return)|
|Recurrence relation|Apa hubungan rekursif antara kasus ke `N` dengan kasus ke `N-1`|

## B. Number Theory

- Cabang matematika yang memplajari bilangan `integer`

### Prime Number
```go
func prime(number int) bool {
    if number < 2 {
        return false
    }
    for i := 2; i < int(math.Sqrt(float64(number))); i++ {
        if number % i == 0 {
            return false
        }
    }
    return true
}
```

### GCD
```go
func gcd(a int, b int) int{
    if b == 0 {
        return a
    }
    return gcd(b,a%b)
}
```

### LCM
```go
func lcm(a int, b int) int {
    return a * (b/gcd(a,b))
}
```

## C. Searching

- Linear search, search yang melakukan iterasi terhadap elemen elemen yang ada
- Builtin search, menggunakan fungsi disediakan oleh bahasa pemrograman untuk menggunakan *searching*

## D. Sorting

- Melakukan pengurutan elemen elemen yang ada menjadi desending/ascending.
- Sorting dapat dilakukan baik `integer` maupun `string`, dan bahkan `slice`
- Sorting dapat melalui *builtin sort* yang disediakan oleh `package sort`atau algoritma sort biasa

|Jenis Sort|Kecepatan|Deskripsi|
|---|---|---|
|Selection Sort|`O(n^2)`|Menggunakan inner loop untuk melakukan sorting dan dilakukan sorting terhadap seluruh indeks|
|Counting Sort|`O(n+k)`|Membuat sebuah array flag yang menyimpan indeks dari array yang akan disort, ketika melakukan sorting, diiterasi terhadap array flag kemudian dilakukan penyusunan ulang berdasarkan indeks flag. Namun teknik ini boros memori|

## E. Stack
- `LIFO` *last in first out* seperti mengambil baju dilemari
- Operasi dasar


|Operasi|Deskripsi|
|---|---|
|`.push()`|memasukan elemen|
|`.pop()`|menghapus elemen teratas|
|`.top()`|mengembalikan nilai elemen teratas|

## F. Queue
- `FIFO` *first in first out* seperti mengantre
- Operasi dasar


|Operasi|Deskripsi|
|---|---|
|`.enqueue()`|memasukan elemen|
|`.dequeue()`|menghapus elemen terdepan|
|`.front()`|mengembalikan nilai elemen terdepan|

## F. Priority Queue
- Menggunakan prisip binary tree heap (BFS)
- Sorting `queue` berdasarkan fungsi evaluasi (default:max())
- Operasi dasar


|Operasi|Deskripsi|
|---|---|
|`.enqueue()`|memasukan elemen|
|`.dequeue()`|menghapus elemen terdepan|
|`.top()`|mengembalikan nilai elemen teratas|

## G. Dequeue
- Mirip seperti `queue` memiliki kemampuan melakukan operasi terhadap elemen terdepan dan terbelakang
- Operasi dasar


|Operasi|Deskripsi|
|---|---|
|`.push_front()`|memasukan elemen ke depan|
|`.push_back()`|memasukan elemen ke belakang|
|`.pop_front()`|menghapus elemen terdepan|
|`.pop_back()`|menghapus elemen terbelakang|
|`.front()`|mengembalikan nilai elemen terdepan|

## H. Set dan Map
- `Set` sebagai kolesi dari elemen berbeda yang terdefinisi
- `Map` merupakan `dictionary` yang artinya mendefinisikan suatu nilai (yang unik) ke sebuah nilai lain sehingga disebut juga *associative container*
- Keduanya dapat diimplementasikan secara *ordered* maupun *unordered*

# Praktikum

[Hasil Submisi]()

## 1. Prima ke-x

[Source code](./praktikum/xthPrime/xthPrime.go)
- Pada fungsi buat special case
- Selain special base maka akan masuk kedalam iterasi *inner loop* `for` yang dibantu flag `check`
- Ide iterasi penggunaan `for` untuk meningkatkan `upper bound` dari iterasi kedua.
- Didalam iterasi kedua terdapat branch yang mana ketika indeks iterasi sampai di cap, maka `check` akan bertambah.
- Jika `check == input` maka akan di return nilai `N` yang merupakan incrementing indeks

![Hasil](./screenshots/prime-x.PNG)

## 2. Fibonacci (Rekursif)

[Source code](./praktikum/fibonacci/fibonacci.go)
- Dalam definisinya `fibonacci`merupakan sequence yang tiap elemennya terbentuk atas penjumlahan elemen ke `N-1` dan ke `N-2`.
```
if N == 0:
    F(N) = 0
else if N == 1:
    F(N) = 1
else:
    F(N) = F(N-1) + F(N-2)
```
- Dari sini kita dapat melihat bahwa bilangan fibonaci dapat diimplementasikan secara rekursif karna memiliki `Base Case` dan `Reccurance Relation`.
- Maka tiap case case yang sudah didefinisi di pseudocode sebelumnya dapat dilakukan return tiap demikian.

![Hasil](./screenshots/fib.png)

## 3. Prima Segi Empat

[Source code](./praktikum/prime-square/primeSquare.go)
- Membuat special case ketika `start < 2 dan start < 3`
- Menggunakan `naive` method untuk mencari bilangan prima
- Ketika prima ditemukan, jumlahkan sekaligus untuk menghitung sumnya
- Melakukan tracking dengan flag `size`, ketika `size == 0` maka loop berhenti
- Cetak elemen yang diappend menggunakan *innerloop* sesuai permintaan

![Hasil](./screenshots/primeSquare.png)

## 4. Total Maksimum Beret Dilangan

[Source code](./praktikum/max-sequence/maxSequence.go)
- Menggunakan metode [berikut](https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/).
- Inisasi nilai `max` dengan nilai paling kecil dan `sum` dengan `0`.
- Iterasi terhadap array integer, tiap awal iterasi jumlahkan elemen ke `sum`.
- Cek apabila nilai `sum` besar dari `max` jika ya, maka masukan nilai `sum` ke `max`. Tidak, maka abaikan.
- Kemudian cek apabila `sum < 0`, jika ya, jadikan `sum = 0` ,karna jika `sum` bernilai negatif maka sudah tidak termaksud sumasi maksimal sehingga `sum` direset ke 0 yang jika selanjutnya diisi nilai negatif, nilai negatif tersebut harapannya besar dari nilai negatif yang tersimpan di `max`.

![Hasil](./screenshots/max-seq.png)

## 5. Min Max Number

[Source code](./praktikum/min-max/minMax.go)
- Menggunakan metode linear search terhadap seluruh elemen
- Nilai indeks dan nilianya akan disimpan kedalam variabel lain
- Setelah iterasi selesai, dibuat string lalu dikembalikan string tersebut

![Hasil](./screenshots/min-max.png)

## 6. Maximum Buy Product

[Source code](./praktikum/max-buy-product/)
- Menggunakan `built-in sort`
- `sort.Slice` menerima operand berupa `slice` dan `anonymous function`
- Operan `anonymous function` akan menjadi argumen yang menghasilkan nilai `bool` berupa apakan suatu elemen itu lebih dari elemen yang lainnya. Nilai `bool` ini nantinya akan dijadikan dasar sorting
- Dilanjutkan dengan iterasi terhadap `slice` yang sudah tersortir.
- Tiap iterasi dilakukan, flag `count` bertambah
- Ketika hasil jumlah elemen tiap iterasi lebih dari `money` maka iterasi akan berhenti dan nilai dari `count` akan dicetak

![Hasil](./screenshots/max-buy.png)

## 7. Playing Domino

- Menggunakan `built-in sort`
- `sort.Slice` menerima operand berupa `slice` dan `anonymous function`
- Operan `anonymous function` akan menjadi argumen yang menghasilkan nilai `bool` berupa apakan jumlah elemen cards besar dari elemen lainnya. Nilai `bool` ini nantinya akan dijadikan dasar sorting.
- Hasil sorting berupa *desceding order* karna return dari operator `>` antara elemen awal dengan elemen selanjutnya. Hal ini akan mempermudah mencari kartu dengan jumlah elemen terbesar.
- Dilanjutkan dengan iterasi terhadap `slice` yang sudah tersortir.
- Menggunakan `if` *statement* ketika salah satu elemen card sama dengan elemen pada *deck* maka kembalikan `slice` kartu tersebut, jika tidak tutup kartu.

![Hasil](./screenshots/play-dom.png)

## 8. Most Appear Item

[Source code](./praktikum/most-appear-item/mostAppearItem.go)
- Menggunakan `built-in` *sorting* untuk `string`
- Dibuat data `slice pair` dan diisi dengan `items` yang berada di indeks pertama, ketika `items` kosong kembalikan `itemInfo`.
- Kemudian dilakukan iterasi terhadap `string slice` yang sudah di *sorting*
- Tiap iterasi dicek, ketika elemen tersebut sama dengan elemen yang dibandingkan, maka `counter` bertambah. Jika tidak, elemen di `append` dan flag `i` meningkat, tanda mengevaluasi elemen selanjutnya.
- Kemudian `itemInfo` dilakukan `sort.Slice` dengan nilai pembanding berupa `bool` yang didapat dari `anonymous function` yang mengecek apakan `count` elemen pertama kecil dari `count` elemen lainnya.
- Kembalikan nilai `itemInfo`

![Hasil](./screenshots/most-appear.png)

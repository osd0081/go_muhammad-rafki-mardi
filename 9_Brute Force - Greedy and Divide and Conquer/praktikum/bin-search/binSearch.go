package main

import (
	"fmt"
)

func BinarySearch(array []int, x int)  {
	l,r := 0,(len(array)-1)
	i := -1
	for l <= r{
		mid := (l+r)/2
		if array[mid] < x {
			l = mid + 1
		}else if array[mid] == x {
			i = mid
			break
		}else{
			r = mid - 1
		}
	}
	fmt.Println(i)
}

func main()  {
	BinarySearch([]int{1,1,3,5,5,6,7},3)
	BinarySearch([]int{1,2,3,5,6,8,10},5)
	BinarySearch([]int{12,15,15,19,24,31,53,59,60},53)
	BinarySearch([]int{12,15,15,19,24,31,53,59,60},100)
}
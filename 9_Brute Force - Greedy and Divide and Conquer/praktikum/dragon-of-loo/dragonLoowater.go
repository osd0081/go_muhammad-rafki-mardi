package main

import (
	"fmt"
	"sort"
)

func DragonOfLoowater(dragonHead,knightHeight []int){

	var res int
	var dragon int

	sort.Slice(dragonHead,func (i,j int) bool {
		return dragonHead[i]<dragonHead[j]
	})

	sort.Slice(knightHeight,func (i,j int) bool {
		return knightHeight[i]<knightHeight[j]
	})

	for _,el1 := range dragonHead{
		for _,el2 := range knightHeight{
			if  el1<=el2 {
				res += el2
				break
			}
		}
		dragon += el1
	}
	if res < dragon {
		fmt.Println("Knight fall")	
	}else {
		fmt.Println(res)	
	}
	
}

func main()  {
	DragonOfLoowater([]int{5,4},[]int{7,8,4})
	DragonOfLoowater([]int{5,10},[]int{5})
	DragonOfLoowater([]int{7,2},[]int{4,3,1,2})
	DragonOfLoowater([]int{7,2},[]int{2,1,8,5})
}
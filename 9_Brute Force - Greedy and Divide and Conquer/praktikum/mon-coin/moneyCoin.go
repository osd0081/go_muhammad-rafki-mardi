package main

import (
	"fmt"
)

func moneyCoins(money int) []int {

	var changes = []int{}

	var coins = [...]int{
		10000,
		5000,
		2000,
		1000,
		500,
		200,
		100,
		50,
		20,
		10,
		1,
	}

	for i := 0; i < len(coins); i++{
		for money - coins[i]>=0{
			money -= coins[i]
			changes = append(changes,coins[i])
		}
	}
	return changes
}

func main()  {
	fmt.Println(moneyCoins(123))
	fmt.Println(moneyCoins(432))
	fmt.Println(moneyCoins(543))
	fmt.Println(moneyCoins(7752))
	fmt.Println(moneyCoins(15321))
}
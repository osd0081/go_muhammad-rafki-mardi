# (9) Brute Force - Greedy and Divide and Conquer

- [Summary](#summary)
- [Praktikum](#praktikum)

# Summary

## A Problem Solving Paradigm
- Pendekatan yang digunakan dalam memecahkan masalah

### a. Compleate Search (Brute Force)

- Pencarian dengan melakukan pemindaian terhadap seluruh data.
- Baiknya digunakan sebagai *last resort*
- Mudah karna cukup instan

### b. Greedy

- Algoritma yang mencari *local optimal*
- *local optimal* memberikan sebuah solusi terbaik secara naif bukan secara komplit
- Biasa digunakan untuk struktur data `node`
- Contoh :
    - Djikstra
    - Huffman
    - Etc

### c. Devide and Conquer

- Pembagian permasalahan menjadi kecil sehingga masalah tersebut dapat terselesaikan dengan mudah dan ketika masalah sudah selesai, maka akan dilakukan penggabungan
- Implementasi bisa dilakukan kepada, misal, mencari nilai pangkat ataupun *search algorithm*
- Contoh:
    - Binary search

### d. Dynamic Programming

- Teknik pendekatan penyelesaian dengan memecah masalah menyadi beberpa sub masalah serta memanfaatkan fakta solusi.
- Misal, fibonacci


|Karakteristik DP|Deskripsi|
|---|---|
|*Overlapping subproblem*|Masalah original tersusun atas submasalah yang lebih kecil|
|*Optimal Substructure Property*|Pada DP, suatu masalah haruslah memiliki struktur masalah & sub-masalah yang konsisten & optimal|
- Pendekatan DP

|Pendekatan|Deskripsi|
|---|---|
|*Top down memoization*|ketika sudah menyelesaikan submasalah, kita tidak perlu menghitung ulang, cukup menyimpan nilai tersebut kedalam sebuah memory, misal rekursif|
|*Bottom up Tabulation*|Menghindari rekursif, pemecahan masalah dimulai dari base case yang mana nilai tersebut sudah langsung merupakan optimal sub masalah|

# Praktikum

## 1. Simple Equation

[Source code](./praktikum/sim-eq/simpleEq.go)

- Menyederhanakan persamaan menjadi satu persamaan `2*(x*y + x*z + y*z)/(x*y*z) == (A*A - C)/B`
- Menggunakan metode *brute force*
- Menggunakan 2 innerloop for, loop pertama *tracking* `x`, kedua `y` dan terakhir `z`
- Ketika ditemukan, cetak nilai dan break semua iterasi
- Menggunakan case ketika penjumlahan tiga elemen lebih dari `A`
- Ketika tidak memenuhi syarat seperti yang dinyatakan cetak `Not found`

![hasil](./screenshots/sim-eq.png)

## 2. Money Coin

[Source code](./praktikum/mon-coin/moneyCoin.go)

- Menggunakan metode *greedy*
- Menyusun nilai `coin` menurun
- Menggunakan *inner loop* loop pertama menggerakan elemen yang berada pada `coin` didalamnya terdapat *loop* lagi yang dimana ketika `money` tidak kecil dari receh maka masukan nilai receh kedalamnya, jika tidak indeks `coin` bertambah

![hasil](./screenshots/mon-coin.png)

## 3. Dragon of Loowater

[Source code](./praktikum/dragon-of-loo/)

- Menggunaka metode *greedy*
- Dilakukan sorting terhadap slice `dragonsHead` dan `knightsHeight`
- Terhadap slice `dragonsHead` dilakukan iterasi yang dialamnya dilakukan iterasi `knightsHeight`.
- Ketika elemen `dragonsHead` kecil dari sama dengan `knightsHeight` lakukan *increment* terhadap `sum` `knightsHeight`

![hasil](./screenshots/dragon-of.png)

## 4. Binary Search Algorithm

[Source code](./praktikum/bin-search/binSearch.go)

- Credit geeksforgeeks
- Menggunakan metode *DnC*
- Pertama diambil indeks ditengah `slice` 
- Dilakukan iterasi hingga batas bawah kurang dari batas atas
- Tiap iterasi, dihitung nilai indeks tengah kemudian di cek elemen `slice` dengan indeks tersebut
    - Ketika nilai `slice` kecil dari nilai yang di cari, maka kita harus mengecek elemen yang berada diatas (diatas indeks tengah)
    - Jika kecil dari maka dibawahnya
    - Jika nilai ditemukan, simpan nilai tersebut
- Cetak hasil algoritma diakhiran

![hasil](./screenshots/bin-search.png)

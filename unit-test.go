func TestCreateUserValid(t *testing.T) {
	var testCases = []struct {
		name       string
		path       string
		expectCode int
		data       map[string]interface{}
	}{
		{
			name:       "create user data",
			path:       "/users",
			expectCode: http.StatusOK,
			data: map[string]interface{}{
				"name":     "Test3",
				"email":    "test3@test.com",
				"password": "test3",
			},
		},
	}

	e := InitEchoTestAPI()
	GenerateDataUsers()

	for _, testCase := range testCases {
		newUser, err := json.Marshal(testCase.data)

		if err != nil {
			t.Errorf("marshalling new user failed")
		}

		request := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(newUser))
		request.Header.Set("Content-Type", "application/json")
		response := httptest.NewRecorder()
		c := e.NewContext(request, response)

		c.SetPath(testCase.path)

		cek := controllers.CreateUserController(c)

		if cek != nil {
			t.Errorf("should not get error, get error: %s", cek)
			return
		}

		if response.Code != testCase.expectCode {
			t.Errorf("should return %d, get: %d", testCase.expectCode, response.Code)
		}

		body := response.Body.String()

		type UserResponse struct {
			Message string      `json:"message" form:"message"`
			Data    models.User `json:"data" form:"data"`
		}

		var user UserResponse

		if err := json.Unmarshal([]byte(body), &user); err != nil {
			assert.Error(t, err, "error")
		}

		expectedName := testCase.data["name"]
		if user.Data.Name != expectedName {
			t.Errorf("user name should be %s, get: %s", expectedName, user.Data.Name)
		}
		expectedEmail := testCase.data["email"]
		if user.Data.Email != expectedEmail {
			t.Errorf("user email should be %s, get: %s", expectedEmail, user.Data.Email)
		}
		expectedPassword := testCase.data["password"]
		if user.Data.Password != expectedPassword {
			t.Errorf("user pasword should be %s, get: %s", expectedPassword, user.Data.Password)
		}
	}
}